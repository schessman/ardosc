package org.tux.ardosc

import org.junit.Test

import org.junit.Assert.*

class TrackTest {

    @Test
    fun getRemoteId() {
    }

    @Test
    fun getType() {
    }

    @Test
    fun getName() {
    }

    @Test
    fun getTrackVolume() {
    }

    @Test
    fun getPanPosition() {
    }

    @Test
    fun getMeter() {
    }

    @Test
    fun getCurrentSendVolume() {
    }

    @Test
    fun getCurrentSendEnable() {
    }

    @Test
    fun getRecEnabled() {
    }

    @Test
    fun getSoloEnabled() {
    }

    @Test
    fun getMuteEnabled() {
    }

    @Test
    fun getStripIn() {
    }

    @Test
    fun getSoloIsolateEnabled() {
    }

    @Test
    fun getSoloSafeEnabled() {
    }

    @Test
    fun getTrackVolumeOnSeekBar() {
    }

    @Test
    fun getSendCount() {
    }
}