/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.Handler
import android.os.Message
import android.widget.Button
import android.widget.LinearLayout
import android.view.View
import android.widget.TextView
import android.support.v4.content.ContextCompat


import java.util.ArrayList

/**
 */

class SendsLayout(context: Context) : LinearLayout(context), View.OnClickListener {

    private var iStripIndex: Int = 0

    private var onChangeHandler: Handler? = null

    private val fwSendGains = ArrayList<FaderView>()
    private val ttbEnables = ArrayList<ToggleTextButton>()
    private val llSends = ArrayList<LinearLayout>()

    private val sendListener = object : FaderView.FaderViewListener {

        override fun onFader(id: Int, pos: Int) {
            val fm = onChangeHandler!!.obtainMessage(SEND_CHANGED, iStripIndex, id, pos)
            onChangeHandler!!.sendMessage(fm)
        }

        override fun onStartFade() {

        }

        override fun onStopFade(id: Int, pos: Int) {

        }
    }

    private val checkedChangeListener = OnClickListener { v ->
        /*
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        val tb = v as ToggleTextButton
        val fm = onChangeHandler!!.obtainMessage(SEND_ENABLED, iStripIndex, tb.id, !tb.toggleState)
        onChangeHandler!!.sendMessage(fm)
    }


    init {
        val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT)
        lp.setMargins(1, 1, 1, 1)
        this.layoutParams = lp
        this.orientation = LinearLayout.VERTICAL
        this.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.SENDS_BACKGROUND))
        this.setPadding(1, 0, 1, 0)

    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    override fun onClick(v: View) {

        val vTag = v.tag as String
        val msgButtonClick: Message
        when (vTag) {
            "close" -> {
                msgButtonClick = onChangeHandler!!.obtainMessage(RESET_LAYOUT)
                onChangeHandler!!.sendMessage(msgButtonClick)
            }
            "prev" -> {
                msgButtonClick = onChangeHandler!!.obtainMessage(PREV_SEND_LAYOUT)
                onChangeHandler!!.sendMessage(msgButtonClick)
            }
            "next" -> {
                msgButtonClick = onChangeHandler!!.obtainMessage(NEXT_SEND_LAYOUT)
                onChangeHandler!!.sendMessage(msgButtonClick)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun init(strip: StripLayout) {
        iStripIndex = strip.remoteId

        val tvSendsDescription = TextView(context)
        tvSendsDescription.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        tvSendsDescription.textSize = 18f
        tvSendsDescription.setPadding(4, 4, 4, 4)
        tvSendsDescription.setTextColor(Color.WHITE)
        tvSendsDescription.tag = "pluginTitle"
        tvSendsDescription.setOnClickListener(this)
        tvSendsDescription.text = "${context.getString(R.string.send_layout_description)} \"${strip.track?.name}\""
        addView(tvSendsDescription)

        val llButtons = LinearLayout(context)
        llButtons.orientation = LinearLayout.HORIZONTAL
        llButtons.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        llButtons.setPadding(0, 0, 0, 32)

        val btnClose = Button(context)
        val bclp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, NAVBUTTON_HEIGHT)
        bclp.setMargins(0, 0, 48, 0)
        btnClose.layoutParams = bclp
        btnClose.setPadding(1, 0, 1, 0)
        btnClose.tag = "close"
        btnClose.setText(R.string.btn_close)
        btnClose.setOnClickListener(this)
        llButtons.addView(btnClose)

        val btnPrev = Button(context)
        btnPrev.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, NAVBUTTON_HEIGHT)
        btnPrev.setPadding(1, 0, 1, 0)
        btnPrev.tag = "prev"
        btnPrev.text = "<"
        btnPrev.setOnClickListener(this)
        llButtons.addView(btnPrev)

        val btnNext = Button(context)
        btnNext.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, NAVBUTTON_HEIGHT)
        btnNext.setPadding(1, 0, 1, 0)
        btnNext.tag = "next"
        btnNext.text = ">"
        btnNext.setOnClickListener(this)
        llButtons.addView(btnNext)

        addView(llButtons)

    }

    private fun addSend(sendIndex: Int, name: String) {
        val llSend = LinearLayout(context)
        llSend.orientation = LinearLayout.HORIZONTAL
        llSend.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        val ttbEnable = ToggleTextButton(context, "", "", Color.CYAN, Color.GRAY)
        val slp = LinearLayout.LayoutParams(80, 32)
        slp.setMargins(6, 2, 12, 2)
        ttbEnable.setPadding(0, 0, 0, 0)
        ttbEnable.layoutParams = slp
        ttbEnable.setAllText(name)
        ttbEnable.isAutoToggle = false
        ttbEnable.id = sendIndex
        ttbEnable.setOnClickListener(checkedChangeListener)
        llSend.addView(ttbEnable)
        ttbEnables.add(ttbEnable)

        val fwSend = FaderView(context)
        fwSend.layoutParams = LinearLayout.LayoutParams(240, 48)
        fwSend.max = 1000
        fwSend.setOrientation(FaderView.Orientation.HORIZONTAL)
        fwSend.id = sendIndex
        fwSend.setProgressColor(ContextCompat.getColor(context, R.color.BUTTON_SEND))
        fwSend.val0 = 782
        fwSend.setListener(sendListener)
        llSend.addView(fwSend)

        fwSendGains.add(fwSend)

        addView(llSend)
        llSends.add(llSend)
    }
    //
    //    private Handler mHandler = new Handler() {
    //
    //        /* (non-Javadoc)
    //         * @see android.os.Handler#handleMessage(android.os.Message)
    //         */
    //        @Override
    //        public void handleMessage(Message msg) {
    //            switch (msg.what) {
    //                case 10:
    //                    break;
    //                case 20:
    //                    int pi = msg.arg1;
    //                    Message fm = onChangeHandler.obtainMessage(SEND_CHANGED, iStripIndex, pi, msg.arg2);
    //                    onChangeHandler.sendMessage(fm);
    //                    break;
    //                case 30:
    //                    break;
    //            }
    //        }
    //    };

    fun setOnChangeHandler(onChangeHandler: Handler) {
        this.onChangeHandler = onChangeHandler
    }

    fun sendChanged(sendIndex: Int, value: Float) {
        if (sendIndex - 1 < fwSendGains.size)
            fwSendGains[sendIndex - 1].progress = (value * 1000).toInt()
    }

    fun sendEnable(sendIndex: Int, state: Float) {
        if (sendIndex - 1 < ttbEnables.size) {
            val cb = ttbEnables[sendIndex - 1]
            cb.toggleState = state > 0
        }
    }

    fun deinit() {
        //        for( ToggleTextButton tb : ttbEnables) {
        //            removeView(tb);
        //        }
        //        ttbEnables.clear();
        //        for( FaderView fw: fwSendGains) {
        //            removeView(fw);
        //        }
        //        fwSendGains.clear();
        for (ll in llSends) {
            ll.removeAllViews()
            removeView(ll)
        }
        llSends.clear()
    }

    fun sendName(sendIndex: Int, name: String) {
        val si = sendIndex - 1
        if (sendIndex > ttbEnables.size) {
            addSend(sendIndex, name)
        } else {
            ttbEnables[si].setAllText(name)
        }

    }

    companion object {

        const val SEND_CHANGED = 98
        const val SEND_ENABLED = 99
        const val RESET_LAYOUT = 199
        const val PREV_SEND_LAYOUT = 197
        const val NEXT_SEND_LAYOUT = 198
        private const val NAVBUTTON_HEIGHT = 36
    }
}
