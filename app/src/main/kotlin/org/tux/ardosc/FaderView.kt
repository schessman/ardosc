/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.view.MotionEvent
import android.view.View
import android.support.v7.widget.AppCompatImageView
import android.support.v4.content.ContextCompat

/**
 * class FaderView -
 * @property param InputParameter?
 * @property max Int
 * @property min Int
 * @property progress Int
 * @property parentWidth Float
 * @property parentHeight Float
 * @property relative Float
 * @property orientation Orientation
 * @property bTopText Boolean
 * @property strTopText String
 * @property bBottomText Boolean
 * @property strBottomText String
 * @property val0 Int
 * @property p Paint
 * @property faderBmpVertical Bitmap
 * @property faderBmpHorizontal Bitmap
 * @property mListener FaderViewListener?
 * @property progressColor Int
 * @constructor
 */

class FaderView(context: Context) : AppCompatImageView(context), View.OnTouchListener {

    var param: ArdourPlugin.InputParameter? = null

    internal var max = 1000
    private var min = 0
    internal var progress = 0
        set(`val`) {
            field = `val`


            this.invalidate()
        }
    private var parentWidth: Float = 0.toFloat()
    private var parentHeight: Float = 0.toFloat()
    private var relative: Float = 0.toFloat()
    private var orientation = Orientation.VERTICAL

    private var bTopText = false
    private var strTopText = ""
    private var bBottomText = false
    private var strBottomText = ""

    var val0 = 0

    private val p: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val faderBmpVertical: Bitmap
    private val faderBmpHorizontal: Bitmap

    private var mListener: FaderViewListener? = null

    private var progressColor = ContextCompat.getColor(getContext(), R.color.fader)

    enum class Orientation {
        VERTICAL,
        HORIZONTAL
    }


    interface FaderViewListener {
        fun onFader(id: Int, pos: Int)
        fun onStartFade()
        fun onStopFade(id: Int, pos: Int)
    }

    fun setListener(l: FaderViewListener) {
        mListener = l
    }

    init {
        setOnTouchListener(this)

        faderBmpVertical = BitmapFactory.decodeResource(resources,
                R.drawable.fader_image)

        faderBmpHorizontal = BitmapFactory.decodeResource(resources,
                R.drawable.fader_image_horizontal)

    }

    fun setOrientation(orientation: Orientation) {
        this.orientation = orientation
    }

    fun setMin(min: Int) {
        this.min = min
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)


        //        int ledheight =  (int)parentHeight / 14;

        val `val` = progress

        if (orientation == Orientation.VERTICAL) {
            val leftEdge = parentWidth / 2
            val rightEdge = parentWidth / 2
            val meterWidth = parentWidth / 6
            val topEdge = (12 + if (bTopText) 24 else 0).toFloat()
            val bottomEdge = (12 + if (bBottomText) 24 else 0).toFloat()

            val fullheight = parentHeight - topEdge - bottomEdge

            val topdb = fullheight - `val`.toFloat() / max * fullheight + topEdge

            // full range background
            p.color = progressColor
            p.strokeWidth = 1f
            p.style = Paint.Style.STROKE
            canvas.drawRect(leftEdge - meterWidth / 6, topEdge, rightEdge + meterWidth / 6, parentHeight - bottomEdge, p)

            // bright bg till volume
            p.color = progressColor
            p.strokeWidth = meterWidth
            canvas.drawLine(leftEdge, topdb, rightEdge, fullheight + topEdge - min.toFloat() / max.toFloat() * fullheight, p)

            if (val0 > 0) {
                // 0dB line
                val top0db = fullheight - val0.toFloat() / max * fullheight + topEdge
                p.strokeWidth = 2f
                canvas.drawLine(12f, top0db, parentWidth - 12, top0db, p)
            }

            // the bitmap
            canvas.drawBitmap(faderBmpVertical, leftEdge - faderBmpVertical.width / 2, topdb - faderBmpVertical.height / 2, null)

            p.strokeWidth = 1f
            // top text
            if (bTopText) {
                p.color = ContextCompat.getColor(context, R.color.BUTTON_PAN)
                p.textSize = 14f
                canvas.drawText(strTopText, 12f, 18f, p)
            }
            // bottom text
            if (bBottomText) {
                p.color = ContextCompat.getColor(context, R.color.BUTTON_PAN)
                p.textSize = 14f
                canvas.drawText(strBottomText, 12f, parentHeight - bottomEdge + 30, p)
            }
        } else {
            val vCenter = parentHeight / 2
            val leftEdge = 12f
            val rightEdge = 12f

            val fullWidth = parentWidth - leftEdge - rightEdge
            val leftdb = `val`.toFloat() / max * fullWidth + leftEdge

            // full range background
            p.color = progressColor
            p.strokeWidth = 1f
            p.style = Paint.Style.STROKE
            canvas.drawRect(leftEdge, vCenter - 2, fullWidth + leftEdge, vCenter + 2, p)

            // bright bg till volume
            p.color = this.progressColor
            p.strokeWidth = vCenter / 2
            canvas.drawLine(leftEdge, vCenter, leftdb, vCenter, p)

            if (val0 > 0) {
                // 0dB line
                val left0db = val0.toFloat() / max * fullWidth + leftEdge
                p.strokeWidth = 2f
                canvas.drawLine(left0db, 4f, left0db, parentHeight - 2, p)
            }

            // the bitmap
            canvas.drawBitmap(faderBmpHorizontal, leftdb - faderBmpHorizontal.width / 2, vCenter - faderBmpHorizontal.height / 2, null)

            if (param != null) {
                p.color = Color.WHITE
                p.strokeWidth = 0f
                p.textSize = 12f
                canvas.drawText(param!!.textFromCurrent, leftEdge + 5, vCenter + 5, p)
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val parentW = View.MeasureSpec.getSize(widthMeasureSpec)
        val parentH = View.MeasureSpec.getSize(heightMeasureSpec)
        this.setMeasuredDimension(parentW, parentH)
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        parentWidth = parentW.toFloat()
        parentHeight = parentH.toFloat()

    }


    override fun onTouch(v: View, event: MotionEvent): Boolean {

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                parent.requestDisallowInterceptTouchEvent(true)

                val p = progress.toFloat() / max.toFloat()
                relative = if (orientation == Orientation.VERTICAL) height.toFloat() - p * height - event.y
                else event.x - p * width
                if (mListener != null) {
                    mListener!!.onStartFade()
                }
            }
            MotionEvent.ACTION_MOVE -> {
                if (orientation == Orientation.VERTICAL) {
                    var newVal = max - (max * (event.y + relative) / height).toInt()
                    if (newVal < 0)
                        newVal = 0
                    if (newVal > max)
                        newVal = max
                    progress = newVal
                    onSizeChanged(width, height, 0, 0)
                } else {
                    var newVal = (max * (event.x - relative) / width).toInt()
                    if (newVal < 0)
                        newVal = 0
                    if (newVal > max)
                        newVal = max
                    progress = newVal
                    onSizeChanged(width, height, 0, 0)
                }
                if (mListener != null) {
                    mListener!!.onFader(this.id, this.progress)
                }
            }
            MotionEvent.ACTION_UP -> {
                parent.parent.requestDisallowInterceptTouchEvent(false)
                if (mListener != null) {
                    mListener!!.onStopFade(this.id, this.progress)
                }
            }

            MotionEvent.ACTION_CANCEL -> {
            }
        }
        return true
    }

    fun setProgressColor(progressColor: Int) {
        this.progressColor = progressColor
    }

    fun setbTopText(bTopText: Boolean) {
        this.bTopText = bTopText
    }

    fun setStrTopText(strTopText: String) {
        this.strTopText = strTopText
        this.bTopText = strTopText != ""
    }

    fun setbBottomText(bBottomText: Boolean) {
        this.bBottomText = bBottomText
    }

    fun setStrBottomText(strBottomText: String) {
        this.strBottomText = strBottomText
        this.bBottomText = strBottomText != ""
    }
}


