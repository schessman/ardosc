/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.ListView

import java.util.ArrayList
import java.util.HashMap

/**
 */

class StripSelectLayout(context: Context) : ListView(context) {

    internal var onClickListener: View.OnClickListener? = null

    fun setRoutes(routes: HashMap<Int, Track>, bank: Bank) {

        val b = Bank()

        for (index in routes.keys) {
            val t = routes[index]
            if (t!!.type == Track.TrackType.AUDIO || t.type == Track.TrackType.BUS || t.type == Track.TrackType.VCA) {
                b.add(t.name!!, t.remoteId, bank.contains(t.remoteId), t.type!!)
            }
        }
        val adapter = TrackAdapter(context, b.strips)

        this.adapter = adapter

    }

    private inner class TrackAdapter(context: Context, strip: ArrayList<Bank.Strip>) : ArrayAdapter<Bank.Strip>(context, 0, strip) {

        @SuppressLint("SetTextI18n")
        override fun getView(position: Int, cnvertView: View?, parent: ViewGroup): View? {
            var convertView = cnvertView
            // Get the data item for this position
            val strip = getItem(position)

            if (strip != null) {
                // Check if an existing view is being reused, otherwise inflate the view
                if (convertView == null) {
                    convertView = LayoutInflater.from(context).inflate(R.layout.strip_select_item, parent, false)
                }
                // Lookup view for data population
                val tvName = convertView!!.findViewById<View>(R.id.checkBox) as CheckBox

                // Populate the data into the template view using the data object
                tvName.text = "${strip.name} - ${Track.getTrackTypeName(strip.type!!)}"
                tvName.tag = strip.id
                tvName.isChecked = strip.enabled
                tvName.setOnClickListener(onClickListener)
                // Return the completed view to render on screen
            }
            return convertView
        }
    }
}
