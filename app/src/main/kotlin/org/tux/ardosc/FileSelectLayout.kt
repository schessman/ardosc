/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView

import java.util.ArrayList

/**
 * class FileSelectLayout - handle file selection
 * @property vonClickListener OnClickListener?
 * @constructor
 */
class FileSelectLayout(context: Context) : ListView(context) {
    var vonClickListener: View.OnClickListener? = null

    fun setFileNames(strings: ArrayList<String>?) {

        val adapter = FileAdapter(context, strings!!)

        this.adapter = adapter
    }

    private inner class FileAdapter(context: Context, tracks: ArrayList<String>) : ArrayAdapter<String>(context, 0, tracks) {

        override fun getView(position: Int, cnvertView: View?, parent: ViewGroup): View {
            var convertView = cnvertView
            // Get the data item for this position
            val file = getItem(position)


            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.file_select_item, parent, false)
            }
            // Lookup view for data population
            val tvName = convertView!!.findViewById<View>(R.id.filename) as TextView

            // Populate the data into the template view using the data object
            tvName.text = file
            tvName.tag = file
            tvName.isClickable = true
            //            tvName.setChecked(track.enabled);
            tvName.setOnClickListener(vonClickListener)
            // Return the completed view to render on screen
            return convertView
        }
    }

}
