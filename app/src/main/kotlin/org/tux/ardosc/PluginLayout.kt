/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Handler
import android.os.Message
import android.support.annotation.RequiresApi
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.Spinner
import android.widget.TextView
import android.support.v4.content.ContextCompat

import java.util.ArrayList

import android.os.Build.VERSION_CODES.M


/**
 */

class PluginLayout(context: Context) : LinearLayout(context), View.OnClickListener {

    private var ttbBypass: ToggleTextButton? = null

    internal var track: Track? = null

    private var onChangeHandler: Handler? = null
    private var pluginDescription: TextView? = null
    private var resetPlugin: Button? = null
    private var currentPlugin: ArdourPlugin? = null
    private var scrollView: ScrollView? = null

    private var bInitEnabled = true

    private val parameterHandler = object : FaderView.FaderViewListener {

        override fun onFader(id: Int, pos: Int) {
            val ip = currentPlugin!!.getParameter(id)
            if (ip!!.flags and 0x02 == 0x02) {
                ip.current = pos.toDouble()
            } else
                ip.setCurrentFromFader(pos, 1000)
            val plargs = arrayOfNulls<Any>(2)
            plargs[0] = currentPlugin!!.getParameter(id)?.parameter_index
            plargs[1] = ip.current
            val fm = onChangeHandler!!.obtainMessage(PLUGIN_PARAMETER_CHANGED, currentPlugin!!.trackId, currentPlugin!!.pluginId, plargs)
            onChangeHandler!!.sendMessage(fm)
        }

        override fun onStartFade() {

        }

        override fun onStopFade(id: Int, pos: Int) {

        }
    }

    @SuppressLint("HandlerLeak")
    private val mHandler = object : Handler() {

        /* (non-Javadoc)
         * @see android.os.Handler#handleMessage(android.os.Message)
         */
        override fun handleMessage(msg: Message) {


            when (msg.what) {
                10 -> {
                }
                20 -> {
                    val pi = msg.arg1
                    val ip = currentPlugin!!.getParameter(pi)
                    if (ip!!.flags and 0x02 == 0x02) {
                        ip.current = msg.arg2.toDouble()
                    } else
                        ip.setCurrentFromFader(msg.arg2, 1000)
                    val plargs = arrayOfNulls<Any>(2)
                    plargs[0] = currentPlugin!!.getParameter(pi)?.parameter_index
                    plargs[1] = ip.current
                    val fm = onChangeHandler!!.obtainMessage(PLUGIN_PARAMETER_CHANGED, currentPlugin!!.trackId, currentPlugin!!.pluginId, plargs)
                    onChangeHandler!!.sendMessage(fm)
                }
                30 -> {
                }
                40 -> {
                    val pis = msg.arg1
                    val ips = currentPlugin!!.getParameter(pis)
                    if (ips!!.current != (msg.obj as Float).toDouble()) {
                        ips.current = (msg.obj as Float).toDouble()
                        val plsargs = arrayOfNulls<Any>(2)
                        plsargs[0] = currentPlugin!!.getParameter(pis)?.parameter_index
                        plsargs[1] = ips.current
                        val fms = onChangeHandler!!.obtainMessage(PLUGIN_PARAMETER_CHANGED, currentPlugin!!.trackId, currentPlugin!!.pluginId, plsargs)
                        onChangeHandler!!.sendMessage(fms)
                    }
                }
            }//                    int pir = msg.arg1;
            //                    ArdourPlugin.InputParameter ipr = currentPlugin.getParameter(pir);
        }
    }

    @RequiresApi(api = M)
    fun initLayout(inlude_request: Boolean, t: Track?) {

        if (track != null && track!!.remoteId == t!!.remoteId)
            return
        removeAllViews()
        this.track = t
        pluginDescription = TextView(context)
        pluginDescription!!.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        pluginDescription!!.textSize = 18f
        pluginDescription!!.setPadding(4, 4, 4, 4)
        pluginDescription!!.setTextColor(Color.WHITE)
        pluginDescription!!.tag = "pluginTitle"
        if (track!!.pluginDescriptors.size > 0) {
            if (track!!.pluginDescriptors.size > 1)
                pluginDescription!!.setOnClickListener(this)
        } else
            pluginDescription!!.setText(R.string.lb_Descr_noFX)
        addView(pluginDescription)

        val btnLayout = LinearLayout(context)
        btnLayout.orientation = LinearLayout.HORIZONTAL
        btnLayout.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        btnLayout.setPadding(0, 0, 0, 16)

        val btnClose = Button(context)
        val bclp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, NAVBUTTON_HEIGHT)
        bclp.setMargins(0, 0, 0, 0)
        btnClose.layoutParams = bclp
        btnClose.setPadding(1, 0, 1, 0)
        btnClose.tag = "close"
        btnClose.setText(R.string.btn_close)
        btnClose.setOnClickListener(this)
        btnLayout.addView(btnClose)

        if (t!!.pluginDescriptors.size > 0) {
            resetPlugin = Button(context)
            resetPlugin!!.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, NAVBUTTON_HEIGHT)
            resetPlugin!!.setText(R.string.btn_reset)
            resetPlugin!!.setPadding(1, 0, 1, 0)
            resetPlugin!!.tag = "resetPlugin"
            resetPlugin!!.setOnClickListener(this)
            btnLayout.addView(resetPlugin)

            ttbBypass = ToggleTextButton(context)
            val bblp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, NAVBUTTON_HEIGHT)
            bblp.setMargins(2, 0, 24, 0)
            ttbBypass!!.setPadding(1, 1, 1, 1)
            ttbBypass!!.layoutParams = bblp
            ttbBypass!!.setPadding(1, 0, 1, 0)
            ttbBypass!!.tag = "bypass"
            ttbBypass!!.setAllText("BYPASS")
            ttbBypass!!.setOnClickListener(this)
            ttbBypass!!.onColor = ContextCompat.getColor(context, R.color.BUTTON_FX)
            btnLayout.addView(ttbBypass)
            ttbBypass!!.isAutoToggle = true

        }

        if (track!!.type != Track.TrackType.MASTER) {
            val btnPrev = Button(context)
            btnPrev.layoutParams = LinearLayout.LayoutParams(48, NAVBUTTON_HEIGHT)
            btnPrev.setPadding(1, 0, 1, 0)
            btnPrev.tag = "prev"
            btnPrev.text = "<"
            btnPrev.setOnClickListener(this)
            btnLayout.addView(btnPrev)

            val btnNext = Button(context)
            btnNext.layoutParams = LinearLayout.LayoutParams(48, NAVBUTTON_HEIGHT)
            btnNext.setPadding(1, 0, 1, 0)
            btnNext.tag = "next"
            btnNext.text = ">"
            btnNext.setOnClickListener(this)
            btnLayout.addView(btnNext)
        }
        addView(btnLayout)

        scrollView = ScrollView(context)
        scrollView!!.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        scrollView!!.isFillViewport = true
        addView(scrollView)

        if (inlude_request) {
            val fm = onChangeHandler!!.obtainMessage(PLUGIN_DESCRIPTOR_REQUEST, track!!.remoteId, 1)
            onChangeHandler!!.sendMessage(fm)
        }

    }

    @SuppressLint("SetTextI18n")
    fun init(pluginId: Int) {
        if (!bInitEnabled)
            return
        if (currentPlugin != null && this.currentPlugin!!.pluginId == pluginId)
            return
        this.currentPlugin = track!!.getPluginDescriptor(pluginId)
        tag = currentPlugin
        pluginDescription!!.text = "(${currentPlugin!!.pluginId}/${track!!.pluginDescriptors.size}) - ${currentPlugin!!.name} - ${track!!.name}"
        resetPlugin!!.id = currentPlugin!!.pluginId
        ttbBypass!!.toggleState = !currentPlugin!!.enabled
        //        int pi = 0;

        scrollView!!.removeAllViews()

        val scroller = LinearLayout(context)
        scroller.orientation = LinearLayout.VERTICAL
        scroller.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        scrollView!!.addView(scroller)


        for (index in currentPlugin!!.parameters.keys) {
            val parameter = currentPlugin!!.parameters[index]
            if (parameter!!.flags and 0x80 == 0x80 && parameter.flags and 0x100 != 0x100) {

                Log.d("PLUGIN", "parameter " + parameter.name + " flags: " + String.format("%x", parameter.flags) +
                        " u/l " + String.format("%.2f", parameter.min) + "/" + String.format("%.2f", parameter.max))


                val pLayout = LinearLayout(context)
                pLayout.orientation = LinearLayout.HORIZONTAL
                pLayout.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, PARAMETER_HEIGHT)

                val parameterName = TextView(context)
                parameterName.layoutParams = LinearLayout.LayoutParams(160, PARAMETER_HEIGHT)
                parameterName.text = parameter.name
                parameterName.setTextColor(Color.WHITE)

                pLayout.addView(parameterName)

                if (parameter.scaleSize == 0) {
                    if (parameter.flags and 64 == 64) {
                        val parameterValue = CheckBox(context)
                        parameterValue.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                        parameterValue.isChecked = parameter.current != 0.0
                        parameterValue.setOnCheckedChangeListener { buttonView, isChecked ->
                            val msg = mHandler.obtainMessage(40, buttonView.tag as Int, 0, (if (isChecked) 1 else 0).toFloat())
                            mHandler.sendMessage(msg)
                        }
                        parameterValue.tag = parameter.parameter_index
                        pLayout.addView(parameterValue)
                    } else {
                        val parameterValue = FaderView(context)
                        parameterValue.param = parameter

                        parameterValue.layoutParams = LinearLayout.LayoutParams(240, PARAMETER_HEIGHT)
                        parameterValue.setOrientation(FaderView.Orientation.HORIZONTAL)
                        parameterValue.id = parameter.parameter_index
                        if (parameter.flags and 0x02 == 0x02) {
                            parameterValue.max = parameter.max.toInt()
                            parameterValue.setMin(parameter.min.toInt())
                            parameterValue.progress = parameter.current.toInt()
                        } else {
                            parameterValue.max = 1000
                            parameterValue.progress = parameter.getFaderFromCurrent(1000)
                        }
                        parameterValue.setListener(parameterHandler)
                        pLayout.addView(parameterValue)
                    }
                } else {
                    val aa = MyAdapter(parameter.scalePoints)
                    val parameterValue = Spinner(context)
                    parameterValue.layoutParams = LinearLayout.LayoutParams(240, PARAMETER_HEIGHT)
                    parameterValue.adapter = aa
                    parameterValue.setPopupBackgroundResource(R.color.VeryDark)
                    parameterValue.setSelection(parameter.getIndexFromScalePointKey(parameter.current.toFloat()))
                    parameterValue.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                            val e = aa.getItem(position)
                            val msg = mHandler.obtainMessage(40, parent.tag as Int, 0, e.key)
                            mHandler.sendMessage(msg)
                        }

                        override fun onNothingSelected(parent: AdapterView<*>) {

                        }

                    }
                    parameterValue.tag = parameter.parameter_index
                    pLayout.addView(parameterValue)
                }
                scroller.addView(pLayout)

            } else {
                if (parameter.flags and 0x80 != 0x80)
                    Log.d("Ardmix", "output parameter found: " + parameter.name)
            }
            //            pi++;
        }
        bInitEnabled = false

    }

    inner class MyAdapter(map: Map<Float, String>) : BaseAdapter() {
        private val mData: ArrayList<*>

        init {
            mData = ArrayList<Map<Float, String>>()
            mData.addAll(listOf(map))
        }

        override fun getCount(): Int {
            return mData.size
        }

        override fun getItem(position: Int): Map.Entry<Float, String> {
            @Suppress("UNCHECKED_CAST")
            return mData[position] as Map.Entry<Float, String>
        }

        override fun getItemId(position: Int): Long {

            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val result: View = convertView ?: LayoutInflater.from(parent.context).inflate(R.layout.spinner_item, parent, false)

            val item = getItem(position)
            val tw = result.findViewById<View>(R.id.itemText) as TextView
            tw.text = item.value
            tw.tag = item.value

            return result
        }
    }


    fun setOnChangeHandler(onChangeHandler: Handler) {
        this.onChangeHandler = onChangeHandler
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onClick(v: View) {
        when (v.tag as String) {
            "resetPlugin" -> onChangeHandler!!.sendMessage(onChangeHandler!!.obtainMessage(PLUGIN_RESET, currentPlugin!!.trackId, currentPlugin!!.pluginId))
            "pluginTitle" -> {
                //                this.removeAllViews();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    this.initLayout(false, track)
                }
                bInitEnabled = true
                if (currentPlugin!!.pluginId == track!!.pluginDescriptors.size) {
                    onChangeHandler!!.sendMessage(onChangeHandler!!.obtainMessage(PLUGIN_DESCRIPTOR_REQUEST, currentPlugin!!.trackId, 1))
                } else {
                    onChangeHandler!!.sendMessage(onChangeHandler!!.obtainMessage(PLUGIN_DESCRIPTOR_REQUEST, currentPlugin!!.trackId, currentPlugin!!.pluginId + 1))
                }
            }
            "close" -> onChangeHandler!!.sendMessage(onChangeHandler!!.obtainMessage(SendsLayout.RESET_LAYOUT))

            "next" -> onChangeHandler!!.sendMessage(onChangeHandler!!.obtainMessage(PLUGIN_NEXT))

            "prev" -> onChangeHandler!!.sendMessage(onChangeHandler!!.obtainMessage(PLUGIN_PREV))

            "bypass" -> onChangeHandler!!.sendMessage(onChangeHandler!!.obtainMessage(PLUGIN_BYPASS, currentPlugin!!.trackId, currentPlugin!!.pluginId, if (!ttbBypass!!.toggleState) 1 else 0))

            else -> {
            }
        }
    }

    companion object {

        const val PLUGIN_PARAMETER_CHANGED = 28
        const val PLUGIN_RESET = 29
        const val PLUGIN_DESCRIPTOR_REQUEST = 22
        const val PLUGIN_NEXT = 31
        const val PLUGIN_PREV = 32
        const val PLUGIN_BYPASS = 35
        private const val NAVBUTTON_HEIGHT = 36

        private const val PARAMETER_HEIGHT = 36
    }
}
