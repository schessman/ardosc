/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import java.util.SortedMap
import java.util.TreeMap

/**
 * class ArdourPlugin - manage ardour plugins
 *
 * @property trackId Int
 * @property pluginId Int
 * @property enabled Boolean
 * @property name String?
 * @property parameters TreeMap<Int, InputParameter>
 * @constructor
 */
class ArdourPlugin(var trackId: Int, var pluginId: Int, enabled: Int) {
    var enabled = false

    var name: String? = null

    val parameters = TreeMap<Int, InputParameter>()

    init {
        this.enabled = enabled == 1
    }

    fun addParameter(index: Int, parameter: InputParameter): InputParameter {
        parameters[index] = parameter
        return parameter
    }

    fun getParameter(pi: Int): InputParameter? {
        return parameters[pi]
    }


    class InputParameter(internal var parameter_index: Int, internal var name: String) {

        internal var type: String? = null
        internal var flags: Int = 0
        internal var min: Float = 0.toFloat()
        internal var max: Float = 0.toFloat()
        internal var current: Double = 0.toDouble()
        internal var printFmt: String? = null
        internal var scaleSize: Int = 0

        internal var scalePoints: SortedMap<Float, String> = TreeMap()

        val textFromCurrent: String
            get() = if (printFmt == null || printFmt!!.isEmpty()) {
                if (flags and 0x02 == 0x02)
                    String.format("%.0f", current)
                else
                    String.format("%.2f", current)
            } else
                String.format(printFmt!!, current)

        fun getFaderFromCurrent(base: Int): Int {

            val range = max - min
            return (base / range * (current - min)).toInt()
        }

        fun setCurrentFromFader(`val`: Int, base: Int) {
            val range = max - min

            current = if ((flags and 0x1) == 0x1) Math.round(range * `val` / base + min).toDouble()
            else (range * `val` / base + min).toDouble()

        }

        fun getIndexFromScalePointKey(key: Float): Int {

            for ((index, key1) in scalePoints.keys.withIndex()) if (key1 == key)
                return index
            return 0
        }

        fun addScalePoint(`val`: Float, name: String) {
            scalePoints[`val`] = name
        }
    }

}
