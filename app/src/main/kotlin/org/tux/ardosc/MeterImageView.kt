/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.support.v7.widget.AppCompatImageView
import android.support.v4.content.ContextCompat

/**
 * class MeterImageView
 * @property parentWidth Float
 * @property parentHeight Float
 * @property meterLevel Int
 * @property p Paint
 */
class MeterImageView : AppCompatImageView {

    private var parentWidth: Float = 0.toFloat()
    private var parentHeight: Float = 0.toFloat()

    private var meterLevel = 0
    private val p: Paint

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        p = Paint(Paint.ANTI_ALIAS_FLAG)
    }

    constructor(context: Context) : super(context) {
        p = Paint(Paint.ANTI_ALIAS_FLAG)
        this.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.VeryDark))

        p.textAlign = Paint.Align.RIGHT
        p.strokeWidth = meterWidth

    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val leftEdge = parentWidth / 2 - 12
        val rightEdge = parentWidth / 2 - 12

        val ledHeight = parentHeight.toInt() / 13

        var `val` = meterLevel
        var lower = 1

        for (i in 0..12) {
            val b = `val` and 1

            var c = Color.rgb(0, 128, 0)

            when {
                i > 11 -> c = Color.RED
                i > 10 -> c = Color.argb(255, 255, 0xbb, 0x33)
                i > 9 -> c = Color.YELLOW
                i > 6 -> c = Color.GREEN
            }

            p.color = c
            if (b == 1 && lower == 1) {
                canvas.drawLine(leftEdge, parentHeight - (i + 1) * ledHeight, rightEdge, parentHeight - (i * ledHeight).toFloat() - 2f, p)
            }

            canvas.drawText(dbs[i], rightEdge + 30, parentHeight - (i * ledHeight).toFloat() - 2f, p)
            `val` = `val` shr 1
            lower = b
        }


    }

    fun setProgress(`val`: Int) {
        meterLevel = `val`
        this.invalidate(0, (parentWidth / 2 - meterWidth).toInt(), (parentWidth / 2 + meterWidth).toInt(), parentHeight.toInt())
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val parentW = MeasureSpec.getSize(widthMeasureSpec)
        val parentH = MeasureSpec.getSize(heightMeasureSpec)
        this.setMeasuredDimension(parentW, parentH)
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        parentWidth = parentW.toFloat()
        parentHeight = parentH.toFloat()

    }

    companion object {

        private const val meterWidth = 10f

        private val dbs = arrayOf("-50", "-40", "-37", "-32", "-27", "-25", "-20", "-17", "-13", "-10", "-5", "-2", "0")
    }
}

