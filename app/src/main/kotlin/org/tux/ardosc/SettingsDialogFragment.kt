/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.EditText

/**
 * Class which provides settings dialog fragment
 */
@SuppressLint("InflateParams")

class SettingsDialogFragment : android.support.v4.app.DialogFragment() {

    private var hostText: EditText? = null
    private var portText: EditText? = null
    private var useOSCbridge: CheckBox? = null
    private var bankSizeText: EditText? = null
    private var useSendsLayoutCheckbox: CheckBox? = null

    override fun onCreateDialog(settingsBundle: Bundle?): Dialog {


        val args = arguments

        // Use the Builder class for convenient dialog construction
        val builder = AlertDialog.Builder(activity)
        val inflater = activity!!.layoutInflater
        val view = inflater.inflate(R.layout.settings_dlg, null)
        builder.setView(view)

        builder.setPositiveButton(R.string.Ok) { _, _ ->
            val host = hostText!!.text.toString()
            var port: Int
            var bankSize: Int

            try {
                port = Integer.parseInt(portText!!.text.toString())
                bankSize = Integer.parseInt(bankSizeText!!.text.toString())
            } catch (e: NumberFormatException) {
                port = 0
                bankSize = 0
            }

            val callingActivity = activity as MainActivity?
            callingActivity!!.onSettingDlg(host, port, bankSize, useSendsLayoutCheckbox!!.isChecked, useOSCbridge!!.isChecked)
        }
        builder.setNegativeButton(R.string.cancel) { _, _ ->
            // User cancelled the dialog
        }
        hostText = view.findViewById<View>(R.id.host) as EditText
        portText = view.findViewById<View>(R.id.port) as EditText
        bankSizeText = view.findViewById<View>(R.id.bankSize) as EditText
        useSendsLayoutCheckbox = view.findViewById<View>(R.id.useSendsLayout) as CheckBox
        useOSCbridge = view.findViewById<View>(R.id.chkOSCbridge) as CheckBox

        hostText!!.setText(args!!.getString("host"))
        portText!!.setText(args.getInt("port").toString())
        bankSizeText!!.setText(args.getInt("bankSize").toString())
        useSendsLayoutCheckbox!!.isChecked = args.getBoolean("useSendsLayout")
        useOSCbridge!!.isChecked = args.getBoolean("useOSCbridge")

        // Create the AlertDialog object and return it
        return builder.create()
    }


}

