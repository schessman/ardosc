/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import java.util.HashSet

class ToggleGroup {

    private val group = HashSet<ToggleListener>()

    fun addToGroup(listener: ToggleListener) {
        group.add(listener)
    }

    /**
     * Toggle the group
     *
     * @param src
     */
    fun toggle(src: ToggleListener) {

        for (listener in group) {

            if (src === listener) {
                listener.toggle()
            }
        }
    }

    /**
     * Toggle the group and set the state on the src.
     * @param src
     */
    fun toggle(src: ToggleImageButton?, state: Boolean) {

        if (src != null) {
            if (src.toggleState == state) {
                return
            }

            for (listener in group) {

                if (src === listener) {
                    listener.toggleState = state
                } else {
                    listener.toggleState = !state
                }
            }
        }
    }

}
