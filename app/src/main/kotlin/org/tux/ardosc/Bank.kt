/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import java.util.ArrayList

/**
 * class Bank - manage the banks
 * @property strips ArrayList<Strip>
 * @property name String?
 * @property button ToggleTextButton?
 * @property stripCount Int
 */

class Bank : Cloneable {

    //    enum BankType { ALL, AUDIO, MIDI, BUS }

    val strips = ArrayList<Strip>()
    var name: String? = null
        set(name) {
            if (button != null)
                button!!.setAllText(name)
            field = name
        }
    //    private BankType type = BankType.ALL;

    var button: ToggleTextButton? = null
        internal set

    val stripCount: Int
        get() = strips.size

    constructor()

    constructor(namep: String) {
        name = namep
    }

    inner class Strip {
        /** remote id of the contained track (1-n)  */

        var id: Int = 0
        var name: String? = null
        var enabled: Boolean = false
        var type: Track.TrackType? = null
    }

    fun getStripPosition(iSendsLayout: Int): Int {
        for (i in strips.indices)
            if (iSendsLayout == strips[i].id)
                return i
        return -1
    }


    fun add(name: String, remoteId: Int, enabled: Boolean, type: Track.TrackType) {
        val strip = Strip()
        strip.id = remoteId
        strip.name = name
        strip.enabled = enabled
        strip.type = type
        var insertIndex = 0
        for (p in strips) {
            if (p.id < remoteId)
                insertIndex++
        }
        strips.add(insertIndex, strip)
    }

    operator fun contains(remoteId: Int): Boolean {
        for (s in strips)
            if (s.id == remoteId)
                return true
        return false
    }

    fun remove(id: Int) {
        for (s in strips) {
            if (s.id == id) {
                strips.remove(s)
                return
            }
        }
    }

}
