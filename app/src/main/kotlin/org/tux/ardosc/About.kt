package org.tux.ardosc

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class About : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        val okButton = findViewById<Button> (R.id.aboutOKbutton)
        okButton.setOnClickListener {
            finish()
        }
    }
}
