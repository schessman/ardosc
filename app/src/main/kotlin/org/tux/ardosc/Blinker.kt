/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.os.Handler
import android.util.Log

/**
 * class Blinker - handle the blink thread
 *
 * @property handler Handler?
 * @property blinkState Boolean
 * @property blinking Boolean
 * @property blinkers HashSet<Blinkable>
 */

internal class Blinker : Thread(), Blinkable {
    override fun startBlink() {this.blinking = true}

    override fun blink() {}

    private var handler: Handler? = null

    private var blinkState = false
    private var blinking = false

    private val blinkers = HashSet<Blinkable>()

    /* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
    override fun run() {

        blinking = true

        while (blinking) {

            blinkState = !blinkState

            try {

                handler!!.sendEmptyMessage(5000)
                Thread.sleep(300)

            } catch (e: InterruptedException) {
                e.printStackTrace()
            }

        }

        Log.d("Ardroid", "Stopping Blinker thread")
    }

    /**
     * Stop the blinker.
     */
    // fun stopBlinker() {
    //     this.blinking = false
    // }


    fun addBlinker(b: ToggleImageButton?) {
        if (b != null) {
            this.blinkers.add(b)
        }
    }


    /**
     * @param handler the handler to set
     */
    fun setHandler(handler: Handler) {
        this.handler = handler
    }


    fun doBlink() {
        for (b in blinkers) {
            b.blink()
        }
    }

}
