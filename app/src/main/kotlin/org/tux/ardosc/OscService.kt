/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.annotation.SuppressLint
import android.os.Handler
import android.os.Message
import android.text.TextUtils
import android.util.Log

import de.sciss.net.OSCClient
import de.sciss.net.OSCListener
import de.sciss.net.OSCMessage

import java.io.IOException
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.SocketAddress
import java.net.UnknownHostException

// import java.util.HashMap

/**
 * class OscService - handle interface to OSC NetUtil
 * @property masterId Int?
 * @property oscClient OSCClient?
 * @property routes HashMap<Int, Track>
 * @property state Int
 * @property host String
 * @property port Int
 * @property protocol String
 * @property transportHandler Handler?
 * @property isConnected Boolean
 * @property queueHandler <no name provided>
 * @property msgCount Int
 * @property msgMax Int
 * @property dropPackages Boolean
 * @property replyListener OSCListener
 * @property master Track
 */
class OscService (var host: String, var port: Int) {

    var masterId: Int = 513

    private var oscClient: OSCClient? = null

    val routes = HashMap<Int, Track>()
    //	private ArrayList<Track> routes= new ArrayList<>();

    private var state = UNINITIALZED

    private var protocol = "udp"


    /** The handler where we shall post transport state updates on the UI thread.  */
    /**
     * @return the transportHandler
     *
     * @parameter transportHandler the transportHandler to set
     */
    var transportHandler: Handler? = null

    /**
     * Check if the OSC client is connected
     * @return Flag indicates OSC connection state
     */
    val isConnected: Boolean
        get() = oscClient != null && oscClient!!.isConnected


    @SuppressLint("HandlerLeak")
    private val queueHandler = object : Handler() {

        override fun handleMessage(queueMessage: Message) {
            val message = queueMessage.obj as OSCMessage
            msgCount++
            if (msgCount > msgMax)
                msgMax = msgCount

            if (message.name == "/rec_enable_toggle") {
                System.out.printf("path: %s\n", message.name)
            }

            if (message.name == "#reply") {
                val arg0 = message.getArg(0) as String
                if (arg0 == "end_route_list") {
                    System.out.printf("#end-route-list\n")

                    val frameRate = message.getArg(1) as Long

                    val msg = transportHandler!!.obtainMessage(OSC_FRAMERATE, frameRate)
                    transportHandler!!.sendMessage(msg)

                    val maxFrame = message.getArg(2) as Long

                    val msg1 = transportHandler!!.obtainMessage(OSC_MAXFRAMES, maxFrame)
                    transportHandler!!.sendMessage(msg1)

                    val msg2 = transportHandler!!.obtainMessage(OSC_STRIPLIST)
                    transportHandler!!.sendMessage(msg2)
                    state = OscService.READY

                    return
                }

                val t = Track()

                when (arg0) {
                    "AT" -> t.type = Track.TrackType.AUDIO
                    "MT" -> t.type = Track.TrackType.MIDI
                    "B" -> t.type = Track.TrackType.BUS
                    "M", "MA" -> {
                        t.type = Track.TrackType.MASTER
                        masterId = message.getArg(6) as Int
                    }
                    "V" -> t.type = Track.TrackType.VCA
                }


                //Set the name of the track
                t.name = message.getArg(1) as String

                //Set mute state
                var i: Int = message.getArg(4) as Int
                t.muteEnabled = i > 0

                //Set solo state
                i = message.getArg(5) as Int
                t.soloEnabled = i > 0

                //Set remote id
                i = message.getArg(6) as Int
                t.remoteId = i


                //Set Volume
                if (message.argCount > 8) {
                    val rawVol = message.getArg(8) as Float
                    t.trackVolume = (rawVol * 1000).toInt()
                }

                if (message.argCount > 9) {
                    t.sendCount = message.getArg(9) as Int
                }

                //Set record state
                if (t.type == Track.TrackType.AUDIO || t.type == Track.TrackType.MIDI) {
                    i = message.getArg(7) as Int
                    t.recEnabled = i > 0
                }

                if (!routes.containsKey(t.remoteId)) {
                    routes[t.remoteId] = t
                    val msg3 = transportHandler!!.obtainMessage(OSC_NEWSTRIP, t)
                    transportHandler!!.sendMessage(msg3)
                }
            } else {

                // get a list of the URI elements
                val pathes = message.name!!.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                var stripIndex: Int? = 0
                val t: Track?

                when (pathes[1]) {
                    "expand" -> if (pathes.size < 3) {
                        System.out.printf("pathes.length < 3: %s, ", message.name)
                        for (a in 0 until message.argCount) {
                            System.out.printf("%d-%s,  ", a, message.getArg(a).toString())
                        }
                        System.out.printf("\n")
                        return
                    }
                    "transport_play" -> transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_PLAY, message.getArg(0) as Int, 0))

                    "transport_stop" -> transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STOP, message.getArg(0) as Int, 0))

                    "rec_enable_toggle" -> transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_RECORD, message.getArg(0) as Int, 0))

                    "select" -> when (pathes[2]) {
                        "send_fader" -> {
                            val margs = arrayOfNulls<Any>(message.argCount)
                            for (i in 0 until message.argCount) {
                                margs[i] = message.getArg(i)
                            }
                            transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_SELECT_SEND_FADER, margs))
                        }

                        "send_enable" -> {
                            val seargs = arrayOfNulls<Any>(message.argCount)
                            for (i in 0 until message.argCount) {
                                seargs[i] = message.getArg(i)
                            }
                            transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_SELECT_SEND_ENABLE, seargs))
                        }

                        "send_name" -> transportHandler!!.sendMessage(transportHandler!!.obtainMessage(
                                OSC_SELECT_SEND_NAME, message.getArg(0) as Int, 0, message.getArg(1)))
                        else -> {
                        }
                    }
                    "strip" -> {
                        // argOffset represents if the strip index is part of the URI (argOffset=1) or not (argOffset=0)
                        var argOffset = 0
                        if (pathes.size > 3 && TextUtils.isDigitsOnly(pathes[3]))
                            stripIndex = Integer.parseInt(pathes[3])
                        else {
                            if (message.argCount > 0) {
                                stripIndex = message.getArg(0) as Int
                                argOffset = 1
                            }
                        }
                        when (pathes[2]) {
                            "list" -> {
                                transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_RECONNECT))
                                Log.d(TAG, "list")
                            }
                            "processors" -> Log.d(TAG, "processors")
                            "plugin" -> when (pathes[3]) {
                                "list" -> {

                                    val plargs = arrayOfNulls<Any>(message.argCount)
                                    for (i in 0 until message.argCount) {
                                        plargs[i] = message.getArg(i)
                                    }
                                    transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_PLUGIN_LIST, plargs))
                                }
                                "descriptor" -> {
                                    val pdargs = arrayOfNulls<Any>(message.argCount)
                                    for (i in 0 until message.argCount) {
                                        pdargs[i] = message.getArg(i)
                                    }
                                    transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_PLUGIN_DESCRIPTOR, pdargs))
                                }
                                "descriptor_end" -> {
                                    val pdeargs = arrayOfNulls<Any>(message.argCount)
                                    for (i in 0 until message.argCount) {
                                        pdeargs[i] = message.getArg(i)
                                    }
                                    transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_PLUGIN_DESCRIPTOR_END, pdeargs))
                                }
                            }
                            "meter" -> {
                                t = getTrack(stripIndex!!)

                                if (message.getArg(1) is Int) {
                                    var newMeter = message.getArg(1) as Int

                                    newMeter = if (newMeter != 0xffff) newMeter and 0x1FFF else 0
                                    if (t != null && t.meter != newMeter) {
                                        t.meter = newMeter
                                        transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STRIP_METER, stripIndex, 0))
                                    }
                                }
                            }
                            "receives" -> {

                                val margs = arrayOfNulls<Any>(message.argCount)
                                for (i in 0 until message.argCount) {
                                    margs[i] = message.getArg(i)
                                }
                                transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STRIP_RECEIVES, margs))
                            }

                            "sends" -> {

                                val rargs = arrayOfNulls<Any>(message.argCount - 1)

                                for (i in 1 until message.argCount) {
                                    rargs[i - 1] = message.getArg(i)
                                }
                                transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STRIP_SENDS, message.getArg(0) as Int, 0, rargs))
                            }
                            "name" -> {
                                t = getTrack(stripIndex!!)
                                if (t != null) {
                                    t.name = message.getArg(argOffset) as String
                                    transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STRIP_NAME, stripIndex, 0))
                                }
                            }
                            "fader" -> {
                                if (pathes.size <= 3) {

                                    t = getTrack(stripIndex!!)
                                    if (t != null && !t.trackVolumeOnSeekBar) {
                                        val `val` = message.getArg(argOffset) as Float
                                        transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STRIP_FADER, stripIndex, Math.round(`val` * 1000)))
                                    } else {
                                        if (t == null) {
                                            Log.d(TAG, "fader change missed" + stripIndex.toString() + "\n")
                                        } else {
                                            Log.d(TAG, "too long path")
                                        }
                                    }
                                }
                            }
                            "recenable" -> {
                                //                                    System.out.printf("OSC rec changed on %d\n", stripIndex);

                                t = getTrack(stripIndex!!)
                                if (t != null) {
                                    t.recEnabled = message.getArg(argOffset) as Float > 0
                                    transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STRIP_REC, stripIndex, 0))
                                } else {
                                    Log.d(TAG, "recEnable missed\n")
                                }
                            }
                            "mute" -> {
                                t = getTrack(stripIndex!!)
                                if (t != null) {
                                    t.muteEnabled = message.getArg(argOffset) as Float > 0
                                    transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STRIP_MUTE, stripIndex, 0))
                                }
                            }
                            "solo" -> {
                                t = getTrack(stripIndex!!)
                                if (t?.type == null) {
                                    Log.d(TAG, "solo")
                                } else {
                                    t.soloEnabled = message.getArg(argOffset) as Float > 0
                                    transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STRIP_SOLO, stripIndex, 0))
                                }
                            }

                            "solo_iso" -> {
                                t = getTrack(stripIndex!!)
                                if (t != null) {

                                    t.soloIsolateEnabled = message.getArg(argOffset) as Float > 0
                                    transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STRIP_SOLOISO, stripIndex, 0))
                                }
                            }

                            "solo_safe" -> {
                                t = getTrack(stripIndex!!)
                                if (t != null) {

                                    t.soloSafeEnabled = message.getArg(argOffset) as Float > 0
                                    transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STRIP_SOLOSAFE, stripIndex, 0))
                                }
                            }

                            "pan_stereo_position" -> {
                                t = getTrack(stripIndex!!)
                                if (t != null && !t.trackVolumeOnSeekBar) {
                                    t.panPosition = message.getArg(argOffset) as Float
                                    transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STRIP_PAN, stripIndex, 0))
                                }
                            }

                            "monitor_input" -> {
                                t = getTrack(stripIndex!!)
                                if (t != null) {
                                    if (message.getArg(argOffset) is Int) {
                                        t.stripIn = message.getArg(argOffset) as Int > 0
                                        transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STRIP_INPUT, stripIndex, 0))
                                    }
                                }
                            }

                            "select" -> transportHandler!!.sendMessage(transportHandler!!.obtainMessage(
                                    OSC_STRIP_SELECT, message.getArg(0) as Int, if (message.getArg(1) as Float > 0) 1 else 0))

                            else -> {
                            }
                        }//								System.out.printf("path: %s, ", message.getName());
                        //								for( int a = 0; a < message.getArgCount(); a++) {
                        //									System.out.printf("%d-%s,  ", a, String.valueOf(message.getArg(a)));
                        //								}
                        //								System.out.printf("\n");
                    }
                    "position" -> when (pathes[2]) {
                        "samples" -> transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_UPDATE_CLOCK, java.lang.Long.parseLong(message.getArg(0) as String)))
                        "smpte" -> transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_UPDATE_CLOCKSTRING, message.getArg(0)))
                    }
                    "master" -> when (pathes[2]) {

                        "meter" -> {
                            t = getTrack(masterId)

                            var newMeter = message.getArg(0) as Int and 0xffff
                            newMeter = if (newMeter != 0xffff) newMeter and 0x1FFF else 0

                            if (t != null && t.meter != newMeter) {
                                t.meter = newMeter
                                transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STRIP_METER, masterId, 0))
                            } else if (t == null)
                                Log.d(TAG, "master meter change missed " + masterId.toString() + "\n")
                        }

                        "fader" -> {
                            t = getTrack(masterId)
                            if (t != null && !t.trackVolumeOnSeekBar) {
                                transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STRIP_FADER,
                                        masterId, Math.round(message.getArg(0) as Float * 1000)))
                            } else {
                                if (t == null)
                                    Log.d(TAG, "master fader change missed " + masterId.toString() + "\n")
                            }
                        }

                        "mute" -> {
                            t = master
                            if (t != null) {
                                t.muteEnabled = message.getArg(0) as Float > 0
                                transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STRIP_MUTE, masterId, (message.getArg(0) as Float).toInt()))
                            }
                        }


                        "name" -> {
                            t = master
                            if (t != null) {
                                t.name = message.getArg(0) as String
                                transportHandler!!.sendMessage(transportHandler!!.obtainMessage(OSC_STRIP_NAME, masterId, 0))
                            }
                        }
                        else -> {
                        }
                    }
                }
            }
            msgCount--

        }
    }

    var msgCount = 0
    var msgMax = 0
    private var dropPackages = false
    /**
     * The OSC reply listener handler
     */
    private val replyListener = OSCListener { message: OSCMessage, _: SocketAddress, _: Long ->
        if (message.name!!.startsWith("/strip/fader")) {
            System.out.printf("path: %s, ", message.name)
            // Log.d(TAG, "messageReceived: " + message.getName());
            for (a in 0 until message.argCount) {
                System.out.printf("%d-%s,  ", a, message.getArg(a).toString())
            }
            print("\n")
        }
        val queueMessage = queueHandler.obtainMessage(999, message)
        if (!dropPackages)
            queueHandler.sendMessage(queueMessage)
    }

    private val master: Track?
        get() = getTrack(masterId)


    //public void setListener(ArdroidMain at){
    //	this.ardroidMainActivity = at;
    //}

    /**
     * Connect to the Ardour OSC server
     */
    fun connect(): Boolean {

        try {

            Log.d(TAG, "Connecting to Ardour")

            if (oscClient != null && oscClient!!.isConnected) {
                disconnect()
            }

            oscClient = OSCClient.newUsing(protocol, port)    // create UDP client with same port number for replies
            oscClient!!.setTarget(InetSocketAddress(InetAddress.getByName(host), port))

            Log.d(TAG, "Starting connection...")

            oscClient!!.start()  // open channel and (in the case of TCP) connect, then start listening for replies

            Log.d(TAG, "Started. Starting listener")



            oscClient!!.addOSCListener(replyListener)

            Log.d(TAG, "Listening.")
            this.state = READY


        } catch (e: UnknownHostException) {
            Log.d(TAG, "Unknown host")
            e.printStackTrace()
            return false
        } catch (e: IOException) {
            Log.d(TAG, "IO Exception")
            e.printStackTrace()
            return false
        }

        return true
    }


    fun requestStripList() {
        if (oscClient!!.isConnected) {

            routes.clear()
            state = OscService.ROUTES_REQUESTED
            //			try {
            //				Thread.sleep(500);
            //			} catch (InterruptedException e) {
            //				e.printStackTrace();
            //			}
            sendOSCMessage("/strip/list")

            val args = arrayOf<Any?>(1)
            sendOSCMessage("/select/expand", args)

            Log.d(TAG, "request strip list")
        }
    }

    /**
     * Method to disconnect from the present connected session.
     * Ask Ardour to stop listening to our observed tracks.
     */
    fun disconnect() {

        Log.d(TAG, "Disconnecting from Ardour")

        //        Object[] sargs = {
        //                0 // bank size
        //                , STRIP_TRACK_AUDIO // strip types
        //                + STRIP_BUS_AUDIO
        //                , 0
        //                , 1 // fader mode (loat from 0 to 1)
        //        };
        //        sendOSCMessage("/set_surface", sargs);
        //
        //        Integer[] args = new Integer[routes.size()];
        //		int j = 0;
        //		for( int index : routes.keySet()) {
        //			args[j++] = index;
        //        }
        //        sendOSCMessage("/strip/ignore", args);

        oscClient!!.dispose()
        state = UNINITIALZED
    }

    /**
     * Map an OSC URI path with the Transport Action in the UI
     * @param cmd The URI of the action to be performed
     */
    fun transportAction(cmd: Int) {

        var uri = ""

        if (state != UNINITIALZED) {
            when (cmd) {
                TRANSPORT_PLAY -> uri = "/transport_play"

                TRANSPORT_STOP -> uri = "/transport_stop"

                GOTO_START -> uri = "/goto_start"

                GOTO_END -> uri = "/goto_end"

                REC_ENABLE_TOGGLE -> uri = "/rec_enable_toggle"

                ALL_REC_ENABLE -> {
                    for (index in routes.keys) {
                        val t = routes[index]
                        if (t?.type == Track.TrackType.AUDIO && !t.recEnabled) {
                            trackListAction(REC_CHANGED, t)
                        }
                    }
                    uri = ""
                }

                ALL_REC_DISABLE -> {
                    for (index in routes.keys) {
                        val t = routes[index]
                        if (t?.type == Track.TrackType.AUDIO && t.recEnabled) {
                            trackListAction(REC_CHANGED, t)
                        }
                    }
                    uri = ""
                }

                ALL_REC_TOGGLE -> {
                    for (index in routes.keys) {
                        val t = routes[index]
                        if (t?.type == Track.TrackType.AUDIO) {
                            trackListAction(REC_CHANGED, t)
                        }
                    }
                    uri = ""
                }

                ALL_STRIPIN_ENABLE -> {
                    for (index in routes.keys) {
                        val t = routes[index]
                        if (t?.type == Track.TrackType.AUDIO && !t.stripIn) {
                            trackListAction(STRIPIN_CHANGED, t)
                        }
                    }
                    uri = ""
                }

                ALL_STRIPIN_DISABLE -> {
                    for (index in routes.keys) {
                        val t = routes[index]
                        if (t?.type == Track.TrackType.AUDIO && t.stripIn) {
                            trackListAction(STRIPIN_CHANGED, t)
                        }
                    }
                    uri = ""
                }

                ALL_STRIPIN_TOGGLE -> {
                    for (index in routes.keys) {
                        val t = routes[index]
                        if (t?.type == Track.TrackType.AUDIO) {
                            trackListAction(STRIPIN_CHANGED, t)
                        }
                    }
                    uri = ""
                }

                ALL_MUTE_ENABLE_TOGGLE -> uri = "/toggle_all_rec_enables"

                LOOP_ENABLE_TOGGLE -> uri = "/ardour/loop_toggle"

                FFWD -> uri = "/ardour/ffwd"

                REWIND -> uri = "/ardour/rewind"

                GOTO_PREV_MARKER -> uri = "/ardour/prev_marker"

                ADD_MARKER -> uri = "/ardour/add_marker"

                GOTO_NEXT_MARKER -> uri = "/ardour/next_marker"
            }
            if (uri != "")
                sendOSCMessage(uri)
        }


    }

    /**
     * Send a track list event to Ardour
     * @param cmd the command to be performed
     * @param i the index of the track receives the action
     * @param keep Flag to control running state on Ardour side
     */
    fun transportAction(cmd: Int, i: Int, keep: Boolean) {

        var uri = ""

        val args = arrayOfNulls<Int>(2)
        args[0] = i

        when (cmd) {
            LOCATE -> {
                uri = "/locate"
                args[1] = if (keep) 1 else 0
            }
        }

        sendOSCMessage(uri, arrayOf(args))
    }

    /*
    / **
    * Send a track list event to Ardour
    * @param track
    * @param position
    * /
    public void trackListVolumeAction( Track track, int position){

    //		String uri = "/ardour/routes/gainabs";
    String uri = "/strip/fader";

    Object[] args = new Object[2];
    args[0] = Integer.valueOf(track.remoteId);

    track.trackVolume = position;
    //args[1] = Double.valueOf(track.sliderToValue(position) / 1000.0);
    args[1] = (float)(position / 1000.0);
    //System.out.printf("pos: %d, args[1]: %f\n", position, (float)args[1]);

    sendOSCMessage(uri, args);
    }
    */


    fun recvListVolumeAction(track: Track, sendId: Int, newVol: Int) {

        val uri = "/strip/send/fader"

        val args = arrayOfNulls<Any>(3)
        args[0] = track.remoteId

        args[1] = sendId

        //		track.trackVolume = position;
        args[2] = newVol / 1000.0

        sendOSCMessage(uri, arrayOf(args))
    }

    fun panAction(track: Track) {

        val uri = "/strip/pan_stereo_position"

        val args = arrayOfNulls<Any>(2)
        args[0] = track.remoteId

        args[1] = track.panPosition

        sendOSCMessage(uri, arrayOf(args))
    }

    /**
     * Send a track list event to Ardour
     * @param cmd The command to be performed on the track
     * @param track the index of the track to take the action
     */
    fun trackListAction(cmd: Int, track: Track) {

        var uri = ""

        var args = arrayOfNulls<Any>(2)
        args[0] = track.remoteId

        var stripURI = "/strip"
        var argValueIndex = 1

        if (track.remoteId == masterId) {
            args = arrayOfNulls(1)
            stripURI = "/master"
            argValueIndex = 0
        }

        when (cmd) {
            REC_CHANGED -> {
                uri = "/recenable"
                args[argValueIndex] = if (track.recEnabled) 0 else 1
            }

            MUTE_CHANGED -> {
                uri = "/mute"
                args[argValueIndex] = if (track.muteEnabled) 0 else 1
            }

            SOLO_CHANGED -> {
                uri = "/solo"
                args[argValueIndex] = if (track.soloEnabled) 0 else 1
            }

            SOLO_ISOLATE_CHANGED -> {
                uri = "/solo_iso"
                args[argValueIndex] = if (track.soloIsolateEnabled) 0 else 1
            }

            SOLO_SAFE_CHANGED -> {
                uri = "/solo_safe"
                args[argValueIndex] = if (track.soloSafeEnabled) 0 else 1
            }

            STRIPIN_CHANGED -> {
                uri = "/monitor_input"
                args[argValueIndex] = if (track.stripIn) 0 else 1
            }

            FADER_CHANGED -> {
                uri = "/fader"
                args[argValueIndex] = track.trackVolume.toFloat() / 1000
            }

            AUX_CHANGED -> {
                args = arrayOfNulls(3)
                stripURI = "/strip/send"
                uri = "/fader"
                args[0] = track.remoteId
                args[1] = track.sourceId
                args[2] = track.currentSendVolume.toFloat() / 1000
            }

            NAME_CHANGED -> {
                uri = "/name"
                args[argValueIndex] = track.name
            }
        }

        sendOSCMessage(stripURI + uri, args)
    }


    /**
     * Send a message with no arguments.
     * @param messageUri The URI of the message to be send
     */
    private fun sendOSCMessage(messageUri: String) {
        this.sendOSCMessage(messageUri, null)
    }


    /**
     * Send an OSC message over the OSC socket.
     * @param messageUri The URI of the message to be send
     * @param args the arguments passed with the message
     */
    private fun sendOSCMessage(messageUri: String, args: Array<Any?>?) {

        val message: OSCMessage = if (args == null || args.isEmpty()) {
            OSCMessage(messageUri)
        } else {
            OSCMessage(messageUri, args)
        }

        try {
            oscClient!!.send(message)
        } catch (e: IOException) {
            Log.d(TAG, "Could not send OSC message: $messageUri")
            e.printStackTrace()
        }

    }


    internal fun getTrack(remoteId: Int): Track? {
        return routes[remoteId]
    }


    fun requestSends(trackIndex: Int) {
        val args = arrayOfNulls<Any>(1)
        args[0] = trackIndex
        this.sendOSCMessage("/strip/sends", arrayOf(args))
    }

    fun requestReceives(trackIndex: Int) {
        val args = arrayOfNulls<Any>(1)
        args[0] = trackIndex
        this.sendOSCMessage("/strip/receives", arrayOf(args))
    }

    fun setProtocol(protocol: String) {
        this.protocol = protocol
    }

    fun setSendEnable(iAuxLayout: Int, source_id: Int, v: Float) {
        val args = arrayOfNulls<Any>(3)
        args[0] = iAuxLayout
        args[1] = source_id
        args[2] = v
        this.sendOSCMessage("/strip/send/enable", arrayOf(args))
    }

    fun requestPlugin(trackIndex: Int, pluginIndex: Int) {

        val args = arrayOfNulls<Any>(2)
        args[0] = trackIndex
        args[1] = pluginIndex

        this.sendOSCMessage("/strip/plugin/descriptor", arrayOf(args))

    }

    fun requestPluginList(trackIndex: Int) {

        val args = arrayOfNulls<Any>(1)
        args[0] = trackIndex

        this.sendOSCMessage("/strip/plugin/list", arrayOf(args))

    }

    fun pluginFaderAction(pluginTrack: Track, pid: Int, ppid: Int, `val`: Double) {
        val args = arrayOfNulls<Any>(4)
        args[0] = pluginTrack.remoteId
        args[1] = pid
        args[2] = ppid
        args[3] = `val`

        this.sendOSCMessage("/strip/plugin/parameter", arrayOf(args))
    }

    fun pluginAction(command: Int, pluginTrack: Track, pid: Int) {
        when (command) {
            PLUGIN_RESET -> {
                val args = arrayOfNulls<Any>(2)
                args[0] = pluginTrack.remoteId
                args[1] = pid
                this.sendOSCMessage("/strip/plugin/reset", arrayOf(args))
            }
        }
    }

    fun trackSendAction(cmd: Int, sendTrack: Track, sendIndex: Int, `val`: Int) {
        when (cmd) {
            SEND_CHANGED -> {
                val args = arrayOfNulls<Any>(3)
                args[0] = sendTrack.remoteId
                args[1] = sendIndex
                args[2] = `val`.toFloat() / 1000
                sendOSCMessage("/strip/send/fader", arrayOf(args))
            }
            SEND_ENABLED -> {
                val eargs = arrayOfNulls<Any>(3)
                eargs[0] = sendTrack.remoteId
                eargs[1] = sendIndex
                eargs[2] = `val`.toFloat()
                this.sendOSCMessage("/strip/send/enable", arrayOf(eargs))
            }
        }
    }

    fun getProcessors(iStripIndex: Int) {
        val eargs = arrayOfNulls<Any>(1)
        eargs[0] = iStripIndex
        this.sendOSCMessage("/strip/processors", arrayOf(eargs))
    }

    fun selectStrip(iStripIndex: Int, state: Boolean) {
        val eargs = arrayOfNulls<Any>(2)

        eargs[0] = iStripIndex
        eargs[1] = if (state) 1 else 0
        this.sendOSCMessage("/strip/select", arrayOf(eargs))
    }

    fun pluginEnable(pluginTrack: Track, pluginId: Int, enabled: Boolean) {
        val eargs = arrayOfNulls<Any>(2)

        eargs[0] = pluginTrack.remoteId
        eargs[1] = pluginId
        this.sendOSCMessage(if (enabled) "/strip/plugin/activate" else "/strip/plugin/deactivate", arrayOf(eargs))
    }

    fun initSurfaceFeedback2() {
        if (oscClient!!.isConnected) {

            val feedback = (FEEDBACK_STRIP_BUTTONS // feedback
                    + FEEDBACK_STRIP_VALUES
                    + FEEDBACK_MASTER
                    + FEEDBACK_STRIP_METER_16BIT
                    + FEEDBACK_TIMECODE
                    + FEEDBACK_TRANSPORT_POSITION_SAMPLES
                    + FEEDBACK_EXTRA_SELECT)

            val args = arrayOf<Any?>(0 // bank size
                    , STRIP_TRACK_AUDIO // strip types
                    + STRIP_HIDDEN
                    + STRIP_BUS_AUDIO
                    + STRIP_MASTER
                    + STRIP_VCA
                    + STRIP_AUX, feedback, 1 // fader mode (loat from 0 to 1)
            )

            sendOSCMessage("/set_surface", args)

        }
    }



    companion object {

        private const val TAG = "OscService"

        // States
        private const val UNINITIALZED = 0
        private const val READY = 1
        private const val ROUTES_REQUESTED = 4

        // Transport actions
        const val TRANSPORT_PLAY = 0
        const val TRANSPORT_STOP = 1
        const val GOTO_START = 2
        const val GOTO_END = 3
        const val REC_ENABLE_TOGGLE = 4
        const val LOOP_ENABLE_TOGGLE = 5
        const val FFWD = 6
        const val REWIND = 7
        const val LOCATE = 8

        const val GOTO_PREV_MARKER = 9
        const val ADD_MARKER = 10
        const val GOTO_NEXT_MARKER = 11

        const val ALL_REC_ENABLE = 20
        const val ALL_REC_DISABLE = 21
        const val ALL_REC_TOGGLE = 22
        private const val ALL_MUTE_ENABLE_TOGGLE = 13

        const val ALL_STRIPIN_ENABLE = 30
        const val ALL_STRIPIN_DISABLE = 31
        const val ALL_STRIPIN_TOGGLE = 32

        // Change Ids
        const val REC_CHANGED = 0
        const val MUTE_CHANGED = 1
        const val SOLO_CHANGED = 2
        const val SOLO_ISOLATE_CHANGED = 51
        const val SOLO_SAFE_CHANGED = 52
        const val NAME_CHANGED = 3
     // const val GAIN_CHANGED = 4
        const val FADER_CHANGED = 6
        const val AUX_CHANGED = 7
     // const val RECEIVE_CHANGED = 8
        const val SEND_CHANGED = 9
        const val SEND_ENABLED = 10


        const val STRIPIN_CHANGED = 5

        const val PLUGIN_RESET = 20
    }
}
