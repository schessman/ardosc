/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/

/**
 *
 */
package org.tux.ardosc

/**
 * Class Track - volume/status control and indicators
 *
 * @property remoteId Int
 * @property type TrackType?
 * @property name String?
 * @property sourceId Int
 * @property trackVolume Int
 * @property panPosition Float
 * @property meter Int
 * @property currentSendVolume Int
 * @property currentSendEnable Boolean
 * @property recEnabled Boolean
 * @property soloEnabled Boolean
 * @property muteEnabled Boolean
 * @property stripIn Boolean
 * @property soloIsolateEnabled Boolean
 * @property soloSafeEnabled Boolean
 * @property trackVolumeOnSeekBar Boolean
 * @property sendCount Int
 * @property pluginDescriptors HashMap<Int, ArdourPlugin>
 */
class Track {

    var remoteId: Int = 0

    var type: TrackType? = null
    var name: String? = null
    var sourceId = 0
    var trackVolume = 0
    var panPosition = 0.5f
    var meter: Int = 0

    var currentSendVolume: Int = 0
    var currentSendEnable: Boolean = false

    var recEnabled = false
    var soloEnabled = false
    var muteEnabled = false
    var stripIn = false
    var soloIsolateEnabled = false
    var soloSafeEnabled = false

    // private
    var trackVolumeOnSeekBar = false
    //helper

    var sendCount = 0

    val pluginDescriptors = HashMap<Int, ArdourPlugin>()


    enum class TrackType {
        MASTER, AUDIO, MIDI, BUS, SEND, RECEIVE, PAN, VCA
    }


    fun addPlugin(pluginIndex: Int, pluginName: String, enabled: Int) {
        val plugin = ArdourPlugin(remoteId, pluginIndex, 1)
        plugin.name = pluginName
        plugin.enabled = enabled > 0
        pluginDescriptors[pluginIndex] = plugin
    }

    fun getPluginDescriptor(pluginIndex: Int): ArdourPlugin? {
        return pluginDescriptors[pluginIndex]
    }

    companion object {

        internal fun getTrackTypeName(type: TrackType): String {
            return when (type) {
                Track.TrackType.AUDIO -> "Audio"
                Track.TrackType.BUS -> "BUS"
                Track.TrackType.MIDI -> "Midi"
                Track.TrackType.SEND -> "Send"
                Track.TrackType.RECEIVE -> "Receive"
                Track.TrackType.PAN -> "Pan"
                Track.TrackType.MASTER -> "Master"
                Track.TrackType.VCA -> "VCA"
                // else -> "unknown track type"
            }
        }
    }


}
