/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Rect
import android.os.*
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.JsonReader
import android.util.JsonWriter
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.HorizontalScrollView
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import de.sciss.net.OSCClient
import java.io.*
import java.util.*
import kotlin.experimental.or
import kotlin.experimental.xor

/**
 * class MainActivity - the top level
 *
 * @property context Context
 * @property oscHost String
 * @property oscPort Int
 * @property bankSize Int
 * @property useSendsLayout Boolean
 * @property useOSCbridge Boolean
 * @property oscService OscService lateinit
 * @property maxFrame Long?
 * @property frameRate Long?
 * @property transportState Byte
 * @property dfBankLoad BankLoadDialog?
 * @property tvClock TextView?
 * @property gotoStartButton ImageButton?
 * @property gotoEndButton ImageButton?
 * @property loopButton ToggleImageButton?
 * @property playButton ToggleImageButton?
 * @property stopButton ToggleImageButton?
 * @property recordButton ToggleImageButton?
 * @property blinker Blinker?
 * @property transportToggleGroup ToggleGroup?
 * @property llStripList LinearLayout?
 * @property strips ArrayList<StripLayout>
 * @property iAuxLayout Int
 * @property iReceiveLayout Int
 * @property iPanLayout Int
 * @property iPluginLayout Int
 * @property iSendsLayout Int
 * @property selectStripId Int
 * @property pluginLayout PluginLayout?
 * @property stripElementMask StripElementMask
 * @property masterStrip StripLayout?
 * @property llMain LinearLayout?
 * @property mainScroller HorizontalScrollView?
 * @property llMaster LinearLayout?
 * @property llBankList LinearLayout?
 * @property sendsLayout SendsLayout?
 * @property banks ArrayList<Bank>
 * @property selectBank Bank?
 * @property currentBank Bank?
 * @property bankId Int
 * @property sl StripLayout?
 * @property topLevelHandler <no name provided>
 * @property routes HashMap<Int, Track>
 */
class MainActivity : AppCompatActivity(), View.OnClickListener, View.OnLongClickListener {

    lateinit var context: Context

    // application settings
    private var oscHost: String = "127.0.0.1"
    private var oscPort = 3819
    private var bankSize = 8
    private var useSendsLayout = false
    private var useOSCbridge: Boolean = false

    lateinit var oscService: OscService


    // Ardour session values
    private var maxFrame: Long? = null
    private var frameRate: Long? = null
    private var transportState = TRANSPORT_STOPPED

    private var dfBankLoad: BankLoadDialog? = null

    // clock
    private var tvClock: TextView? = null

    // top level IO elements
    private var gotoStartButton: ImageButton? = null
    private var gotoEndButton: ImageButton? = null

    private val loopButton: ToggleImageButton? = null
    private var playButton: ToggleImageButton? = null
    private var stopButton: ToggleImageButton? = null
    private var recordButton: ToggleImageButton? = null
    private var blinker: Blinker? = null

    private var transportToggleGroup: ToggleGroup? = null

    private var llStripList: LinearLayout? = null
    private val strips = ArrayList<StripLayout>()

    // some layouts for Sends, Receives, Panning, FX may be get more
    private var iAuxLayout = -1
    private var iReceiveLayout = -1
    private var iPanLayout = -1
    private var iPluginLayout = -1
    private var iSendsLayout = -1

    private var selectStripId = -1


    private var pluginLayout: PluginLayout? = null

    private val stripElementMask = StripElementMask()

    private var masterStrip: StripLayout? = null
    private var llMain: LinearLayout? = null
    private var mainScroller: HorizontalScrollView? = null
    private var llMaster: LinearLayout? = null
    private var llBankList: LinearLayout? = null
    private var sendsLayout: SendsLayout? = null

    // some elements for strip banking
    private val banks = ArrayList<Bank>()
    private var selectBank: Bank? = null
    private var currentBank: Bank? = null
    private var bankId: Int = 0

    private var sl: StripLayout? = null

    @SuppressLint("HandlerLeak")
    private val topLevelHandler = object : Handler() {

        override fun handleMessage(msg: Message) {

            when (msg.what) {

                5000 // perform blink
                -> blinker!!.doBlink()

                StripLayout.STRIP_FADER_CHANGED -> {
                    val ltrack = oscService.getTrack(msg.arg1)

                    if (ltrack != null) {
                        oscService.trackListAction(OscService.FADER_CHANGED, ltrack)
                    }
                }

                SendsLayout.RESET_LAYOUT -> resetLayouts()

                SendsLayout.NEXT_SEND_LAYOUT -> {
                    var nl = currentBank!!.getStripPosition(iSendsLayout)
                    if (nl++ < currentBank!!.strips.size - 1) {
                        resetLayouts()
                        enableSendsLayout(currentBank!!.strips[nl].id, true)
                    }
                }

                SendsLayout.PREV_SEND_LAYOUT -> {
                    var pl = currentBank!!.getStripPosition(iSendsLayout)
                    if (pl-- > 0) {
                        resetLayouts()
                        enableSendsLayout(currentBank!!.strips[pl].id, true)
                    }
                }

                SendsLayout.SEND_CHANGED -> {
                    val sendTrack = oscService.getTrack(msg.arg1)
                    if (sendTrack != null) oscService.trackSendAction(OscService.SEND_CHANGED, sendTrack, msg.arg2, msg.obj as Int)
                }

                SendsLayout.SEND_ENABLED -> {
                    val sendEnableTrack = oscService.getTrack(msg.arg1)
                    if (sendEnableTrack != null) oscService.trackSendAction(OscService.SEND_ENABLED, sendEnableTrack, msg.arg2, if (msg.obj as Boolean) 1 else 0)
                }

                StripLayout.AUX_CHANGED -> {
                    val auxTrack = oscService.getTrack(msg.arg1)
                    if (auxTrack != null) oscService.trackListAction(OscService.AUX_CHANGED, auxTrack)
                }

                StripLayout.RECEIVE_CHANGED -> {
                    sl = getStripLayoutId(iAuxLayout)
                    if (sl != null) {
                        val receiveTrack = oscService.getTrack(sl!!.remoteId)
                        if (receiveTrack != null) {
                            oscService.recvListVolumeAction(receiveTrack, msg.arg1, msg.arg2)
                        }
                    }
                }

                StripLayout.PAN_CHANGED -> {
                    val panTrack = oscService.getTrack(msg.arg1)
                    if (panTrack != null) oscService.panAction(panTrack)
                }

                PluginLayout.PLUGIN_PARAMETER_CHANGED -> {
                    val pluginTrack = oscService.getTrack(msg.arg1)
                    if (pluginTrack != null) {
                        @Suppress("UNCHECKED_CAST") val plargs = msg.obj as Array<Any>
                        oscService.pluginFaderAction(pluginTrack, msg.arg2, plargs[0] as Int, plargs[1] as Double)
                    }
                }

                PluginLayout.PLUGIN_DESCRIPTOR_REQUEST -> oscService.requestPlugin(msg.arg1, msg.arg2)

                PluginLayout.PLUGIN_BYPASS -> {
                    val pluginTrack2 = oscService.getTrack(msg.arg1)
                    if (pluginTrack2 != null) {
                        oscService.pluginEnable(pluginTrack2, msg.arg2, msg.obj as Int == 1)
                    }
                }

                PluginLayout.PLUGIN_RESET -> {
                    val pluginTrack1 = oscService.getTrack(msg.arg1)
                    if (pluginTrack1 != null) {
                        oscService.pluginAction(OscService.PLUGIN_RESET, pluginTrack1, msg.arg2)
                        resetLayouts()
                    }
                }

                PluginLayout.PLUGIN_NEXT -> {
                    var np = currentBank!!.getStripPosition(iPluginLayout)
                    if (np++ < currentBank!!.strips.size - 1) {
                        enablePluginLayout(currentBank!!.strips[np].id, true)
                    }
                }

                PluginLayout.PLUGIN_PREV -> {
                    var pp = currentBank!!.getStripPosition(iPluginLayout)
                    if (pp-- > 0) {
                        enablePluginLayout(currentBank!!.strips[pp].id, true)
                    }
                }


                OSC_FRAMERATE -> frameRate = msg.obj as Long

                OSC_MAXFRAMES -> maxFrame = msg.obj as Long

                OSC_STRIPLIST -> {
                    updateStripList()
                    oscService.initSurfaceFeedback2()
                }

                OSC_RECONNECT -> startConnectionToArdour()

                OSC_NEWSTRIP -> addStrip(msg.obj as Track)

                OSC_STRIP_NAME -> {
                    sl = getStripLayout(msg.arg1)
                    if (sl != null) sl!!.nameChanged()
                }

                OSC_STRIP_REC -> {
                    sl = getStripLayout(msg.arg1)
                    if (sl != null) sl!!.recChanged()
                }

                OSC_STRIP_MUTE -> {
                    sl = getStripLayout(msg.arg1)
                    if (sl != null)

                        sl!!.muteChanged()
                }

                OSC_STRIP_SOLO -> {
                    sl = getStripLayout(msg.arg1)
                    if (sl != null) sl!!.soloChanged()
                }

                OSC_STRIP_SOLOSAFE -> {
                    sl = getStripLayout(msg.arg1)
                    if (sl != null) sl!!.soloSafeChanged()
                }

                OSC_STRIP_SOLOISO -> {
                    sl = getStripLayout(msg.arg1)
                    if (sl != null) sl!!.soloIsoChanged()
                }

                OSC_STRIP_INPUT -> {
                    sl = getStripLayout(msg.arg1)
                    if (sl != null) sl!!.inputChanged()
                }

                OSC_STRIP_PAN -> {
                    sl = getStripLayout(msg.arg1)
                    if (sl != null) sl!!.panChanged()
                }

                OSC_STRIP_FADER -> {
                    sl = getStripLayout(msg.arg1)
                    if (sl != null) {
                        sl!!.track!!.trackVolume = msg.arg2
                        sl!!.volumeChanged()
                    }
                }

                OSC_STRIP_RECEIVES -> {
                    @Suppress("UNCHECKED_CAST") val args = msg.obj as Array<Any>

                    val remoteId = args[0] as Int
                    if (args.size % 5 == 0) {
                        var i = 0
                        while (i < args.size) {
                            sl = getStripLayout(args[i] as Int)
                            if (sl != null) sl!!.setType(Track.TrackType.SEND, args[i + 3] as Float, args[i + 2] as Int, args[i + 4] as Int == 1)
                            i += 5
                        }
                    } else {
                        if (iReceiveLayout == remoteId) {
                            var i = 1
                            while (i < args.size) {
                                sl = getStripLayout(args[i] as Int)
                                if (sl != null) sl!!.setType(Track.TrackType.SEND, args[i + 3] as Float, args[i + 2] as Int, args[i + 4] as Int == 1)
                                i += 5
                            }
                        }
                    }
                }

                OSC_STRIP_SELECT -> selectStripId = if (msg.arg2 == 1) msg.arg1 else -1

                OSC_STRIP_SENDS -> {
                    @Suppress("UNCHECKED_CAST") val sargs = msg.obj as Array<Any>

                    sl = getStripLayout(msg.arg1)

                    if (!useSendsLayout && iAuxLayout == msg.arg1) {
                        var i = 0
                        while (i < sargs.size) {
                            if (sargs[i] as Int > 0) {
                                val receiveStrip = getStripLayout(sargs[i] as Int)
                                receiveStrip?.setType(Track.TrackType.RECEIVE, sargs[i + 3] as Float, sargs[i + 2] as Int, sargs[i + 4] as Int == 1)
                            }
                            i += 5
                        }
                    } else {
                        if (iAuxLayout == msg.arg1) showSends(sl)
                    }
                }

                OSC_SELECT_SEND_FADER -> {
                    sl = getStripLayoutId(iSendsLayout)
                    if (sendsLayout != null && sl != null && selectStripId == sl!!.remoteId) {
                        val sfargs = msg.obj as Array<*>
                        sendsLayout!!.sendChanged(sfargs[0] as Int, sfargs[1] as Float)
                    }
                }

                OSC_SELECT_SEND_ENABLE -> {
                    sl = getStripLayoutId(iSendsLayout)
                    if (sendsLayout != null && sl != null && selectStripId == sl!!.remoteId) {
                        val sfargs = msg.obj as Array<*>
                        sendsLayout!!.sendEnable(sfargs[0] as Int, sfargs[1] as Float)
                    }
                }

                OSC_SELECT_SEND_NAME -> {
                    sl = getStripLayoutId(iSendsLayout)
                    if (sendsLayout != null && sl != null && selectStripId == sl!!.remoteId) {
                        val newName = msg.obj as String
                        if (newName != " ") sendsLayout!!.sendName(msg.arg1, msg.obj as String)
                    }
                }


                OSC_STRIP_METER -> {
                    sl = getStripLayout(msg.arg1)
                    if (sl != null) {
                        sl!!.meterChange()
                    }
                }

                // 0171/3532161

                OSC_PLUGIN_LIST -> {
                    val plargs = msg.obj as Array<*>
                    val track = oscService.getTrack(plargs[0] as Int)
                    //                    track.pluginDescriptors.clear();
                    var pli = 1
                    while (pli < plargs.size) {
                        if (track != null) {
                            if (!track.pluginDescriptors.containsKey(plargs[pli])) {
                                track.addPlugin(plargs[pli] as Int, plargs[pli + 1] as String, plargs[pli + 2] as Int)
                            }
                        }
                        pli += 3
                    }
                    if (track != null) {
                        showPluginLayout(track)
                    }
                }

                OSC_PLUGIN_DESCRIPTOR -> {
                    val pdargs = msg.obj as Array<*>
                    val stripIndex = pdargs[0] as Int
                    val pluginId = pdargs[1] as Int
                    val t = oscService.getTrack(stripIndex)

                    if (t != null) {
                        val pluginDes = t.getPluginDescriptor(pluginId)
                        //                        pluginDes.getParameters().clear();
                        //                        pluginDes.enabled = ((int)pdargs[2] == 1);
                        var pi = 2
                        val parameter: ArdourPlugin.InputParameter
                        parameter = when {
                            pdargs[2] as Int > pluginDes!!.parameters.size -> ArdourPlugin.InputParameter(pdargs[pi] as Int, pdargs[pi + 1] as String)
                            else -> pluginDes.getParameter(pdargs[2] as Int)!!
                        }
                        parameter.flags = pdargs[pi + 2] as Int
                        parameter.type = pdargs[pi + 3] as String
                        parameter.min = pdargs[pi + 4] as Float
                        parameter.max = pdargs[pi + 5] as Float
                        parameter.printFmt = pdargs[pi + 6] as String
                        parameter.scaleSize = pdargs[pi + 7] as Int
                        for (spi in 0 until parameter.scaleSize) {
                            parameter.addScalePoint(pdargs[pi + 8] as Float, pdargs[pi + 9] as String)
                            pi += 2
                        }
                        parameter.current = pdargs[pi + 8] as Double
                        if (!pluginDes.parameters.containsKey(pdargs[2])) {
                            pluginDes.addParameter(pdargs[2] as Int, parameter)
                        }
                        //showPlugin(pluginId, true);
                    }
                }
                OSC_PLUGIN_DESCRIPTOR_END -> {
                    val pdeargs = msg.obj as Array<*>
                    val pt = oscService.getTrack(pdeargs[0] as Int)
                    showPlugin(pt!!, pdeargs[1] as Int, true)
                }

                OSC_UPDATE_CLOCK -> {
                    val clock = msg.obj as Long

                    if (transportState.toInt() == (transportState.toInt() and RECORD_ENABLED.toInt())) {
                        maxFrame = clock
                    }
                }

                OSC_UPDATE_CLOCKSTRING -> {
                    val strClock = msg.obj as String

                    tvClock!!.text = strClock
                }

                OSC_RECORD -> if (msg.arg1 == 1) {
                    transportState = (transportState or RECORD_ENABLED)
                    recordButton!!.setToggleState(true, true)
                } else {
                    transportState = (transportState xor RECORD_ENABLED)
                    recordButton!!.setToggleState(false, false)
                }
                OSC_PLAY -> if (msg.arg1 == 1) {
                    if (RECORD_ENABLED.toInt() == (RECORD_ENABLED.toInt() and transportState.toInt())) {
                        transportState = (TRANSPORT_RUNNING or RECORD_ENABLED)
                        recordButton!!.toggleOn()
                    } else {
                        transportState = TRANSPORT_RUNNING

                    }
                    transportToggleGroup!!.toggle(playButton, true)
                } else {
                    if (RECORD_ENABLED.toInt() == (RECORD_ENABLED.toInt() and transportState.toInt())) {
                        transportState = (TRANSPORT_RUNNING xor RECORD_ENABLED)
                        recordButton!!.toggleOff()
                    } else {
                        transportState = transportState xor TRANSPORT_RUNNING

                    }
                    transportToggleGroup!!.toggle(playButton, false)
                }

                OSC_STOP -> if (msg.arg1 == 1) {
                    transportState = 0
                }
            }//                    sbLocation.setProgress(Math.round(( (float) clock/ (float) maxFrame) * 10000));
            //                    sbLocation.refreshDrawableState();
            //                    sbLocation.setProgress(Math.round(( (float) clock/ (float) maxFrame) * 10000));
            //                    sbLocation.refreshDrawableState();
        }

    }

    val routes: HashMap<Int, Track>
        get() = oscService.routes

    override fun onCreate(savedInstanceState: Bundle?) {
        context = this
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        // enable networking in main thread
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        // restore preferences
        val settings = getSharedPreferences(TAG, 0)
        oscHost = settings.getString("oscHost", "127.0.0.1") //if oscHost setting not found default to 127.0.0.1
        oscPort = settings.getInt("oscPort", 3819) //if oscPort setting not found default to 3819
        bankSize = settings.getInt("bankSize", 8)
        useSendsLayout = settings.getBoolean("useSendsLayout", false)
        useOSCbridge = settings.getBoolean("useOSCbridge", false)

        stripElementMask.loadSettings(settings)

        tvClock = findViewById(R.id.str_clock)

        mainScroller = findViewById(R.id.main_scroller)
        llStripList = findViewById(R.id.strip_list)
        llMain = findViewById(R.id.main_layout)
        llMaster = findViewById(R.id.master_view)

        //Create the transport button listeners
        gotoStartButton = this.findViewById(R.id.bGotoStart)
        gotoStartButton!!.setOnClickListener(this)

        gotoEndButton = this.findViewById(R.id.bGotoEnd)
        gotoEndButton!!.setOnClickListener(this)

        playButton = this.findViewById(R.id.bPlay)
        playButton!!.setOnClickListener(this)
        playButton!!.isAutoToggle = false

        stopButton = this.findViewById(R.id.bStop)
        stopButton!!.setOnClickListener(this)
        stopButton!!.isAutoToggle = false
        stopButton!!.toggle() //Set stop to toggled state

        recordButton = this.findViewById(R.id.bRec)
        recordButton!!.setOnClickListener(this)
        recordButton!!.isAutoToggle = false

        transportToggleGroup = ToggleGroup()

        transportToggleGroup!!.addToGroup(playButton!!)
        transportToggleGroup!!.addToGroup(stopButton!!)
        //        transportToggleGroup.addToGroup(loopButton);

        //        sbLocation = (SeekBar) this.findViewById(R.id.locationBaR);
        //        sbLocation.setMax(10000);
        //        sbLocation.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
        //
        //                @Override
        //                public void onStopTrackingTouch(SeekBar arg0) {
        //                }
        //
        //                @Override
        //                public void onStartTrackingTouch(SeekBar arg0) {
        //                }
        //
        //                @Override
        //                public void onProgressChanged(SeekBar sb, int pos, boolean fromUser) {
        //
        //                    if (fromUser){
        //
        //                        if (!(RECORD_ENABLED == (transportState & RECORD_ENABLED)
        //                        )){
        //// && TRANSPORT_RUNNING == (transportState & TRANSPORT_RUNNING)
        //
        //                            int loc = Math.round(( sbLocation.getProgress() * maxFrame) / 10000);
        //                            oscService.transportAction(OscService.LOCATE, loc, TRANSPORT_RUNNING == (transportState & TRANSPORT_RUNNING) );
        //
        //                            transportState = TRANSPORT_STOPPED;
        //
        //                            transportToggleGroup.toggle(stopButton, true);
        //                            recordButton.toggleOff();
        //                        }
        //                    }
        //                }
        //            }
        //        );
    }

    /**
     * Activity is has become visible
     * @see android.app.Activity.onResume
     */
    override fun onResume() {

        super.onResume()
        Log.d(TAG, "Resuming...")

        if (!oscService.isConnected) {
            Log.d(TAG, "Not connected to OSC server... Connect")
            this.startConnectionToArdour()
        }

        blinker = Blinker()
        blinker!!.setHandler(topLevelHandler)
        blinker!!.addBlinker(recordButton)
        blinker!!.start()

    }

    public override fun onStop() {
        super.onStop()
    }

    public override fun onStart() {
        super.onStart()
        if (!::oscService.isInitialized) {
            oscService = OscService(this.oscHost, oscPort)
            oscService.setProtocol(if (useOSCbridge) OSCClient.TCP else OSCClient.UDP)
            oscService.transportHandler = topLevelHandler
        }
    }

    override fun onDestroy() {
        stopConnectionToArdour()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        when (id) {

            R.id.action_connection -> {
                resetLayouts()
                val dfSettings = SettingsDialogFragment()
                val settingsBundle = Bundle()
                settingsBundle.putString("host", oscHost)
                settingsBundle.putInt("port", oscPort)
                settingsBundle.putInt("bankSize", bankSize)
                settingsBundle.putBoolean("useSendsLayout", useSendsLayout)
                settingsBundle.putBoolean("useOSCbridge", useOSCbridge)
                dfSettings.arguments = settingsBundle
                dfSettings.show(supportFragmentManager, "Connection Settings")
            }
            R.id.action_mask -> stripElementMask.config(this)
            R.id.action_connect -> startConnectionToArdour()
            R.id.action_disconnect -> stopConnectionToArdour()
            R.id.action_about -> aboutActivity ()

            // Banking menu
            R.id.action_newbank -> newBank()
            R.id.action_editbank -> editBank(banks.indexOf(currentBank))
            R.id.action_removebank -> removeBank(banks.indexOf(currentBank))
            R.id.action_savebank -> saveBank(currentBank)
            R.id.action_loadbank -> loadBank()

            // Record button menu
            R.id.action_allrecenable -> oscService.transportAction(OscService.ALL_REC_ENABLE)

            R.id.action_allrecdisable -> oscService.transportAction(OscService.ALL_REC_DISABLE)

            R.id.action_allrectoggle -> oscService.transportAction(OscService.ALL_REC_TOGGLE)

            // Strip In menu
            R.id.action_allstripin_enable -> oscService.transportAction(OscService.ALL_STRIPIN_ENABLE)

            R.id.action_allstripin_disable -> oscService.transportAction(OscService.ALL_STRIPIN_DISABLE)

            R.id.action_allstripin_toggle -> oscService.transportAction(OscService.ALL_STRIPIN_TOGGLE)
        }

        return super.onOptionsItemSelected(item)
    }

    private fun newBank() {
        val dfBankSetting = BankSettingDialogFragment()
        val bankBundle = Bundle()
        bankBundle.putInt("bankIndex", -1)
        dfBankSetting.arguments = bankBundle
        dfBankSetting.show(supportFragmentManager, "Bank Settings")
    }

    internal fun loadBank() {
        val dir = File(context.filesDir.path)
        val files = dir.listFiles()

        val mapFileNames = HashMap<String, String>()
        for (f in files) {
            if (f.name.endsWith(".bank")) {
                mapFileNames[f.name] = f.absolutePath
                Log.d(TAG, "filename: " + f.absolutePath)
            }
        }
        val bankBundle = Bundle()
        bankBundle.putStringArrayList("files", ArrayList(mapFileNames.keys))
        dfBankLoad = BankLoadDialog()
        bankBundle.putInt("bankIndex", -1)
        dfBankLoad!!.arguments = bankBundle
        dfBankLoad!!.show(supportFragmentManager, "Load Bank")

    }

    private fun saveBank(bank: Bank?) {
        if (bank != null) try {

            val stringWriter = StringWriter()
            val jsonWriter = JsonWriter(stringWriter)
            jsonWriter.beginObject()
            jsonWriter.name("Bank").value(bank.name)
            jsonWriter.name("Strips")
            jsonWriter.beginArray()
            for (strip in bank.strips) {

                jsonWriter.beginObject().name(strip.name).value(strip.id.toLong())
                jsonWriter.endObject()

            }
            jsonWriter.endArray()

            jsonWriter.endObject()
            jsonWriter.close()

            print(stringWriter.toString())
            val outputStream: FileOutputStream
            val strFilename = bank.name!! + ".bank"
            outputStream = openFileOutput(strFilename, Context.MODE_PRIVATE)
            outputStream.write(stringWriter.toString().toByteArray())
            outputStream.close()

        } catch (e: IOException) {
            e.printStackTrace()
        }


    }

    fun removeBank(iBankIndex: Int) {
        if (iBankIndex > 0) {
            val lbnk = banks[iBankIndex]
            val dialogClickListener = DialogInterface.OnClickListener { _, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        llBankList!!.removeView(lbnk.button)
                        banks.remove(lbnk)
                        showBank(0)
                    }
                }
            }
            val builder = AlertDialog.Builder(context)
            builder.setMessage("Are you sure to remove the current bank?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show()
        }
    }

    fun onSettingDlg(host: String, port: Int, bankSize: Int, useSendsLayout: Boolean, useOSCbridge: Boolean) {
        this.oscHost = host
        this.oscPort = port
        this.bankSize = bankSize
        this.useSendsLayout = useSendsLayout
        this.useOSCbridge = useOSCbridge
        savePreferences()
    }

    private fun savePreferences() {

        val settings = getSharedPreferences(TAG, 0)
        val editor = settings.edit()

        editor.putString("oscHost", oscHost)
        editor.putInt("oscPort", oscPort)
        editor.putInt("bankSize", bankSize)
        editor.putBoolean("useSendsLayout", useSendsLayout)
        editor.putBoolean("useOSCbridge", useOSCbridge)

        editor.apply()
    }

    private fun aboutActivity () {
    // bring up the about activity
        startActivity (Intent (this, About :: class.java) )
    }

    private fun stopConnectionToArdour() {

        if (oscService.isConnected) {
            oscService.disconnect()
        }
        llStripList!!.removeAllViews()
        strips.clear()
        llMaster!!.removeView(masterStrip)
        if (llBankList != null) llBankList!!.removeAllViews()
        banks.clear()
    }

    private fun startConnectionToArdour() {
        oscService.host = oscHost
        oscService.port = oscPort
        oscService.setProtocol(if (useOSCbridge) OSCClient.TCP else OSCClient.UDP)

        stopConnectionToArdour()

        if (!oscService.connect()) {
            val builder = AlertDialog.Builder(context)
            builder.setMessage("Failed to connect to ardour at $oscHost").setPositiveButton("Ok", null).show()
        }


        oscService.requestStripList()
    }

    fun addStrip(t: Track) {

        val stripLayout = StripLayout(this, t)
        val stripLP = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)

        stripLayout.setPadding(0, 0, 0, 0)
        stripLayout.layoutParams = stripLP
        stripLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.fader))


        stripLayout.id = strips.size + 1

        stripLayout.setOnClickListener(this)
        stripLayout.setOnChangeHandler(topLevelHandler)

        stripLayout.init(context, stripElementMask)

        if (t.type == Track.TrackType.MASTER) {
            masterStrip = stripLayout

        }
        strips.add(stripLayout)

        System.out.printf("adding strip %s with id %d\n", t.name, t.remoteId)

    }

    fun updateStripList() {

        if (banks.size == 0) {
            createBanklist()
            updateBanklist()
            showBank(0)
        }

    }

    private fun createBanklist() {

        //        int iTrackInBank = 0;
        //        int iBusBegin = 1;
        //        int iBusEnd = 1;
        //        Track.TrackType lt = Track.TrackType.AUDIO;

        for (b in banks) {
            b.strips.clear()
        }

        banks.add(Bank("All"))

        val allBank = banks[0]
        /*        Bank nb = new Bank();
        nb.setType(Bank.BankType.AUDIO);
        banks.add(nb);*/

        for (sl in strips) {
            val t = sl.track

            if (t?.type != Track.TrackType.MASTER) {
                allBank.add(t?.name!!, sl.id, true, t.type!!)

                /*                if ( lt != t.type || iTrackInBank++ == bankSize) {
                    nb = new Bank();
                    banks.add(nb);
                    switch (t.type) {
                        case AUDIO:
                            nb.setType(Bank.BankType.AUDIO);
                            break;
                        case BUS:
                            nb.setType(Bank.BankType.BUS);
                            break;
                        case MIDI:
                            nb.setType(Bank.BankType.MIDI);
                            break;
                    }
                    iTrackInBank = 0;
                }

                nb.add(t.name, t.remoteId, true);
                lt = t.type;*/
            }
        }

        /*        for( Bank b: banks) {
            switch(b.getType()) {
                case AUDIO:
                    b.setName(String.format("IN %d-%d", iBusEnd, iBusEnd + (b.getStrips().size())-1));
                    iBusEnd += b.getStrips().size() ;
                    break;
                case BUS:
                    b.setName(String.format("BUS %d-%d", iBusBegin, iBusBegin + (b.getStrips().size())-1));
                    iBusBegin += b.getStrips().size() - 1;
                    break;
            }
        }*/

    }

    private fun updateBanklist() {

        bankId = 1000 //llBankList.generateViewId();
        llBankList = findViewById(R.id.bank_list)

        llBankList!!.removeAllViews()
        for (iBankIndex in banks.indices) {
            val lbank = banks[iBankIndex]
            lbank.button = ToggleTextButton(this)
            val bankLP = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 32)
            bankLP.setMargins(1, 1, 1, 1)
            //            bankLP.gravity = Gravity.RIGHT;
            lbank.button!!.layoutParams = bankLP
            lbank.button!!.setPadding(0, 0, 0, 0)
            lbank.button!!.setAllText(lbank.name)
            lbank.button!!.tag = iBankIndex
            lbank.button!!.id = bankId + iBankIndex + 1
            lbank.button!!.setOnClickListener(this)
            lbank.button!!.setOnLongClickListener(this)
            lbank.button!!.isAutoToggle = true
            lbank.button!!.toggleState = false
            llBankList!!.addView(lbank.button)
        }
        val ttbAddBank = ToggleTextButton(this)
        val bankLP = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 32)
        bankLP.setMargins(1, 1, 1, 1)
        bankLP.gravity = Gravity.END
        ttbAddBank.layoutParams = bankLP
        ttbAddBank.setPadding(0, 0, 0, 0)
        ttbAddBank.setAllText("+")
        ttbAddBank.id = bankId
        ttbAddBank.setOnClickListener(this)
        ttbAddBank.isAutoToggle = false
        ttbAddBank.toggleState = false
        llBankList!!.addView(ttbAddBank)

    }

    override fun onClick(v: View) {

        when (v.id) {

            R.id.bGotoStart -> {
                if (transportState == TRANSPORT_RUNNING) {
                    transportToggleGroup!!.toggle(stopButton, true)
                    transportState = TRANSPORT_STOPPED
                } else if (TRANSPORT_RUNNING.toInt() == (transportState.toInt() and TRANSPORT_RUNNING.toInt()) && RECORD_ENABLED.toInt() == (transportState.toInt() and RECORD_ENABLED.toInt())) {
                } else oscService.transportAction(OscService.GOTO_START)


            }

            R.id.bGotoEnd -> {
                if (transportState == TRANSPORT_RUNNING) {
                    transportToggleGroup!!.toggle(this.stopButton, true)
                    transportState = TRANSPORT_STOPPED
                } else if (TRANSPORT_RUNNING.toInt() == (transportState.toInt() and TRANSPORT_RUNNING.toInt()) && RECORD_ENABLED.toInt() == (transportState.toInt() and RECORD_ENABLED.toInt())) {
                } else oscService.transportAction(OscService.GOTO_END)
            }

            R.id.bPlay -> {
                oscService.transportAction(OscService.TRANSPORT_PLAY)
                if (RECORD_ENABLED.toInt() == (RECORD_ENABLED.toInt() and transportState.toInt())) {
                    transportState = (TRANSPORT_RUNNING or RECORD_ENABLED)
                    recordButton!!.toggleOn()
                } else {
                    transportState = TRANSPORT_RUNNING

                }
                transportToggleGroup!!.toggle(playButton, true)
            }

            R.id.bStop -> {
                val wasRunning = transportState.toInt() == (TRANSPORT_RUNNING.toInt() or RECORD_ENABLED.toInt())
                oscService.transportAction(OscService.TRANSPORT_STOP)

                transportState = TRANSPORT_STOPPED
                transportToggleGroup!!.toggle(stopButton, true)
                if (wasRunning) recordButton!!.toggleOff()
            }

            R.id.bRec -> {
                oscService.transportAction(OscService.REC_ENABLE_TOGGLE)
                if (RECORD_ENABLED.toInt() != (transportState.toInt() and RECORD_ENABLED.toInt())) {
                    if (TRANSPORT_STOPPED.toInt() == (transportState.toInt() and TRANSPORT_STOPPED.toInt())) {
                        recordButton!!.toggleOnAndBlink()
                    } else {
                        recordButton!!.toggleOn()
                    }
                    transportState = (transportState or RECORD_ENABLED)
                } else {
                    transportState = (transportState xor RECORD_ENABLED)
                    recordButton!!.toggleOff()
                }
            }

            R.id.bLoopEnable -> if (!(TRANSPORT_RUNNING.toInt() == (transportState.toInt() and TRANSPORT_RUNNING.toInt()) && RECORD_ENABLED.toInt() == (transportState.toInt() and RECORD_ENABLED.toInt()))) {

                oscService.transportAction(OscService.LOOP_ENABLE_TOGGLE)
                transportToggleGroup!!.toggle(loopButton, true)
            }

            else -> {
                val i = v.id

                if (i - 1 >= bankId && i - 1 < bankId + banks.size) {
                    showBank(v.tag as Int)
                } else if (i == bankId) {
                    newBank()
                } else {

                    val sl = getStripLayoutId(i)
                    if (sl != null) {
                        when (v.tag as String) {
                            "strip" -> showStripDialog(sl.remoteId)
                            "rec" -> if (TRANSPORT_RUNNING.toInt() != (transportState.toInt() and TRANSPORT_RUNNING.toInt())) oscService.trackListAction(OscService.REC_CHANGED, oscService.getTrack(sl.remoteId)!!)

                            "mute" -> if (sl.showType == Track.TrackType.RECEIVE) {
                                val b = v as ToggleTextButton
                                val asl = getStripLayoutId(iAuxLayout)
                                if (asl != null) oscService.setSendEnable(asl.remoteId, sl.track?.sourceId!!, if (b.toggleState) 1f else 0f)
                            } else if (sl.showType == Track.TrackType.SEND) {
                                val b = v as ToggleTextButton
                                oscService.setSendEnable(sl.remoteId, oscService.getTrack(sl.remoteId)?.sourceId!!, if (b.toggleState) 1f else 0f)
                            } else oscService.trackListAction(OscService.MUTE_CHANGED, oscService.getTrack(sl.remoteId)!!)

                            "solo" -> oscService.trackListAction(OscService.SOLO_CHANGED, oscService.getTrack(sl.remoteId)!!)

                            "soloiso" -> oscService.trackListAction(OscService.SOLO_ISOLATE_CHANGED, oscService.getTrack(sl.remoteId)!!)

                            "solosafe" -> oscService.trackListAction(OscService.SOLO_SAFE_CHANGED, oscService.getTrack(sl.remoteId)!!)

                            "input" -> oscService.trackListAction(OscService.STRIPIN_CHANGED, oscService.getTrack(sl.remoteId)!!)

                            "in" -> enableSendOnFader(i, (v as ToggleTextButton).toggleState)

                            "aux" -> if (!useSendsLayout) enableReceiveOnFader(i, (v as ToggleTextButton).toggleState)
                            else enableSendsLayout(i, (v as ToggleTextButton).toggleState)

                            "pan" -> enablePanFader(i, (v as ToggleTextButton).toggleState)

                            "fx" -> enablePluginLayout(i, (v as ToggleTextButton).toggleState)
                        }
                    }
                }
            }
        }
    }

    override fun onLongClick(v: View): Boolean {

        editBank(v.tag as Int)
        return true
    }

    private fun enableSendsLayout(iStripId: Int, bState: Boolean) {
        val strip = getStripLayoutId(iStripId)
        if (strip != null && bState) {

            resetLayouts()

            sendsLayout = SendsLayout(this)
            sendsLayout!!.init(strip)
            sendsLayout!!.setOnChangeHandler(topLevelHandler)
            llStripList!!.addView(sendsLayout, strip.position + 1)

            strip.setBackgroundColor(ContextCompat.getColor(context, R.color.SENDS_BACKGROUND))
            strip.pushVolume()
            iSendsLayout = iStripId
            forceVisible(sendsLayout!!)

            oscService.selectStrip(strip.remoteId, true)
        } else {
            if (!useSendsLayout) {
                for (sl in strips) {
                    if (sl.showType == Track.TrackType.SEND || sl.showType == Track.TrackType.RECEIVE) {
                        sl.resetType()
                        if (currentBank == null || !/* + 1 */currentBank!!.contains(sl.id)) llStripList!!.removeView(sl)
                    }
                }
            } else {
                if (sendsLayout != null) {
                    sendsLayout!!.deinit()
                    llStripList!!.removeView(sendsLayout)
                    sendsLayout = null
                }
            }
            if (strip != null) {
                strip.resetBackground()
                strip.sendChanged(false)
                strip.pullVolume()
            }
            iSendsLayout = -1
        }
    }

    private fun showSends(strip: StripLayout?) {
        //        resetLayouts();
        sendsLayout = SendsLayout(this)
        //        sendsLayout.setRotation(90);
        sendsLayout!!.init(strip!!)
        sendsLayout!!.setOnChangeHandler(topLevelHandler)
        llStripList!!.addView(sendsLayout, strip.position + 1)
        forceVisible(sendsLayout!!)
    }

    private fun enablePanFader(iStripId: Int, bState: Boolean) {
        val strip = getStripLayoutId(iStripId)

        if (strip != null) {
            if (bState) {

                resetLayouts()

                iPanLayout = iStripId
                strip.setBackgroundColor(ContextCompat.getColor(context, R.color.BUTTON_PAN))
                strip.setType(Track.TrackType.PAN, 0f, 0, false)
                strip.panChanged()

            } else {
                strip.resetPan()
                strip.resetBackground()

                iPanLayout = -1
                strip.volumeChanged()
            }
        }
    }

    private fun enableSendOnFader(iStripId: Int, bState: Boolean) {
        val strip = getStripLayoutId(iStripId)
        if (strip != null) {
            if (bState) {
                resetLayouts()

                oscService.requestReceives(strip.remoteId)

                strip.setBackgroundColor(ContextCompat.getColor(context, R.color.BUS_AUX_BACKGROUND))
                iReceiveLayout = iStripId
            } else {
                for (receiveLayout in strips) {
                    if (receiveLayout.showType == Track.TrackType.SEND || receiveLayout.showType == Track.TrackType.RECEIVE) {
                        receiveLayout.resetType()
                        if (currentBank == null || !currentBank!!.contains(receiveLayout.id)) llStripList!!.removeView(receiveLayout)
                    }
                }
                strip.resetBackground()
                oscService.getTrack(strip.remoteId)!!.recEnabled = false
                strip.recChanged()
                iReceiveLayout = -1
            }
        }
    }

    private fun enableReceiveOnFader(iStripId: Int, bState: Boolean) {
        val strip = getStripLayoutId(iStripId)
        if (strip != null) {
            if (bState) {

                resetLayouts()
                oscService.requestSends(strip.remoteId)

                strip.setBackgroundColor(ContextCompat.getColor(context, R.color.BUS_AUX_BACKGROUND))
                strip.pushVolume()
                iAuxLayout = iStripId
            } else {
                for (sl in strips) {
                    if (sl.showType == Track.TrackType.SEND || sl.showType == Track.TrackType.RECEIVE) {
                        sl.resetType()
                        if (currentBank == null || !currentBank!!.contains(sl.id)) llStripList!!.removeView(sl)
                    }
                }
                strip.resetBackground()
                strip.sendChanged(false)
                strip.pullVolume()
                iAuxLayout = -1
            }
        }
    }

    private fun enablePluginLayout(iStripId: Int, bState: Boolean) {
        val strip = getStripLayoutId(iStripId)
        if (strip != null) {
            if (bState) {

                resetLayouts()
                pluginLayout = PluginLayout(this)
                pluginLayout!!.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                pluginLayout!!.orientation = LinearLayout.VERTICAL
                pluginLayout!!.setBackgroundColor(ContextCompat.getColor(context, R.color.PLUGIN_BACKGROUND))
                pluginLayout!!.setPadding(1, 0, 1, 0)
                //            pluginLayout.setId(iStripId);
                pluginLayout!!.setOnChangeHandler(topLevelHandler)
                //place layout in strip list
                if (strip.remoteId == oscService.masterId)
                // its the master strip
                    llStripList!!.addView(pluginLayout)
                else llStripList!!.addView(pluginLayout, strip.position + 1)


                iPluginLayout = iStripId
                // we are ready to receive plugin list
                oscService.requestPluginList(strip.remoteId)
                strip.setBackgroundColor(ContextCompat.getColor(context, R.color.PLUGIN_BACKGROUND))
            } else {
                if (pluginLayout != null) {
                    pluginLayout!!.removeAllViews()
                    llStripList!!.removeView(pluginLayout)
                }
                strip.fxOff()
                strip.resetBackground()

                pluginLayout = null
                iPluginLayout = -1
            }
        }
    }

    private fun showPluginLayout(track: Track) {
        if (pluginLayout != null) {
            if (pluginLayout!!.track == null || pluginLayout!!.track!!.remoteId == track.remoteId) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    pluginLayout!!.initLayout(true, track)
                }
                if (track.pluginDescriptors.size == 0) forceVisible(pluginLayout!!)
            }
        }
    }

    private fun forceVisible(v: View) {
        v.post {
            val rectScrollBounderies = Rect()
            mainScroller!!.getDrawingRect(rectScrollBounderies)
            val sl = mainScroller!!.scrollX
            if (v.right - rectScrollBounderies.right > 0) mainScroller!!.smoothScrollTo(v.right - rectScrollBounderies.right + sl, 0)
        }
    }

    private fun showPlugin(t: Track, pluginId: Int, bState: Boolean) {
        if (bState) {
            if (pluginLayout == null) resetLayouts()
            else {
                if (pluginLayout!!.track!!.remoteId == t.remoteId) {
                    pluginLayout!!.init(pluginId)
                    //                pluginLayout.setTag(pluginDes);
                    forceVisible(pluginLayout!!)
                }
            }
        } else {
            if (pluginLayout != null) {
                val sl = getStripLayout(pluginLayout!!.id)
                sl?.fxOff()
                llStripList!!.removeView(pluginLayout)
                pluginLayout = null
            }
        }
    }

    private fun resetLayouts() {

        if (iAuxLayout != -1) enableReceiveOnFader(iAuxLayout, false)

        if (iReceiveLayout != -1) enableSendOnFader(iReceiveLayout, false)

        if (iPanLayout != -1) enablePanFader(iPanLayout, false)

        if (iPluginLayout != -1) enablePluginLayout(iPluginLayout, false)

        if (iSendsLayout != -1) enableSendsLayout(iSendsLayout, false)

    }

    private fun showStripDialog(iStripIndex: Int) {
        val t = oscService.getTrack(iStripIndex)
        val dfStripSetting = StripSettingDialogFragment()
        val settingsBundle = Bundle()
        settingsBundle.putString("stripName", t!!.name)
        settingsBundle.putInt("stripIndex", iStripIndex)
        if (t.type == Track.TrackType.AUDIO) {
            settingsBundle.putBoolean("stripIn", t.stripIn)
            settingsBundle.putBoolean("stripRecord", t.recEnabled)
        }
        settingsBundle.putBoolean("stripMute", t.muteEnabled)
        if (t.type != Track.TrackType.MASTER) {
            settingsBundle.putBoolean("stripSolo", t.soloEnabled)
            settingsBundle.putBoolean("stripSoloIso", t.soloIsolateEnabled)
            settingsBundle.putBoolean("stripSoloSafe", t.soloSafeEnabled)
        }

        dfStripSetting.arguments = settingsBundle
        dfStripSetting.show(supportFragmentManager, "Strip Settings")

    }

    private fun getStripLayout(iRemoteid: Int): StripLayout? {
        for (sl in strips) {
            if (sl.remoteId == iRemoteid) return sl
        }
        return null
    }

    private fun getStripLayoutId(id: Int): StripLayout? {
        for (sl in strips) {
            if (sl.id == id) return sl
        }
        return null
    }

    fun onStripDlg(iStripIndex: Int, strName: String, bStripIn: Boolean, bRecord: Boolean, bMute: Boolean, bSolo: Boolean, bSoloIso: Boolean, bSoloSafe: Boolean) {
        val t = oscService.getTrack(iStripIndex)
        t!!.name = strName
        oscService.trackListAction(OscService.NAME_CHANGED, oscService.getTrack(iStripIndex)!!)
        if (t.muteEnabled != bMute) oscService.trackListAction(OscService.MUTE_CHANGED, oscService.getTrack(iStripIndex)!!)
        if (t.type != Track.TrackType.MASTER) {
            if (t.soloEnabled != bSolo) oscService.trackListAction(OscService.SOLO_CHANGED, oscService.getTrack(iStripIndex)!!)
            if (t.soloIsolateEnabled != bSoloIso) oscService.trackListAction(OscService.SOLO_ISOLATE_CHANGED, oscService.getTrack(iStripIndex)!!)
            if (t.soloSafeEnabled != bSoloSafe) oscService.trackListAction(OscService.SOLO_SAFE_CHANGED, oscService.getTrack(iStripIndex)!!)
            if (t.type != Track.TrackType.BUS) {
                if (t.stripIn != bStripIn) oscService.trackListAction(OscService.STRIPIN_CHANGED, oscService.getTrack(iStripIndex)!!)
                if (t.recEnabled != bRecord) oscService.trackListAction(OscService.REC_CHANGED, oscService.getTrack(iStripIndex)!!)
            }
        }
    }

    fun onBankDlg(iBankIndex: Int, bank: Bank) {
        if (selectBank != null && iBankIndex != -1) {
            selectBank!!.name = bank.name
            selectBank!!.strips.clear()
            for (strip in bank.strips) {
                val sl = getStripLayout(strip.id)
                if (sl != null) selectBank!!.add(strip.name!!, sl.id, strip.enabled, strip.type!!)
            }
            showBank(selectBank!!.button!!.tag as Int)
        } else {
            for (strip in bank.strips) {
                val sl = getStripLayout(strip.id)
                if (sl != null) strip.id = sl.id
            }
            banks.add(bank)
            updateBanklist()
            showBank(bank.button!!.tag as Int)
        }
    }

    private fun editBank(iBankIndex: Int) {
        if (iBankIndex > -1) {
            selectBank = banks[iBankIndex]
            val dlg = BankSettingDialogFragment()
            val settingsBundle = Bundle()
            settingsBundle.putString("bankName", selectBank!!.name)
            settingsBundle.putInt("bankIndex", iBankIndex)
            dlg.arguments = settingsBundle
            dlg.show(supportFragmentManager, "Bank Settings")
        }
    }

    fun getBank(iBankIndex: Int): Bank {
        val result = Bank("new bank")
        if (iBankIndex != -1) {
            val clone = banks[iBankIndex]
            result.name = clone.name
            for (s in clone.strips) {
                val sl = getStripLayout(s.id)
                if (sl != null) result.add(s.name!!, sl.remoteId, false, s.type!!)
            }
            result.button = clone.button
        }
        return result
    }

    private fun showBank(iBankIndex: Int) {
        resetLayouts()
        val lbankButton: ToggleTextButton = llBankList!!.getChildAt(iBankIndex) as ToggleTextButton

        val lbank = banks[iBankIndex]
        llStripList!!.removeAllViews()

        for (i in banks.indices) {
            banks[i].button!!.toggleState = false
        }

        if (stripElementMask.stripSize == StripLayout.AUTO_STRIP) {
            stripElementMask.autoSize = llMain!!.width / (lbank.stripCount + 1)
        }

        var c = 0
        for (bankStrip in lbank.strips) {
            val sl = getStripLayoutId(bankStrip.id)
            if (sl != null) {
                sl.init(this.context, stripElementMask)
                llStripList!!.addView(sl, c)
                sl.position = c++
            }
        }

        llMaster!!.removeAllViews()
        if (masterStrip != null) {
            masterStrip!!.init(this.context, stripElementMask)
            llMaster!!.addView(masterStrip)
        }


        lbankButton.toggleOn()
        currentBank = lbank
    }

    fun loadBankFile(tag: Any) {
        if (dfBankLoad != null) {
            dfBankLoad!!.dismiss()
            val bank = Bank()
            var nBytesRead: Int
            try {
                val inputStream: FileInputStream = openFileInput(tag as String)
                val content = StringBuilder()
                val buffer = ByteArray(1024)
                nBytesRead = inputStream.read(buffer)
                while (nBytesRead != -1) {
                    content.append(String(buffer, 0, nBytesRead))
                    nBytesRead = inputStream.read(buffer)
                }
                inputStream.close()
                Log.d(TAG, "load file content: " + content.toString())

                val stringReader = StringReader(content.toString())
                val reader = JsonReader(stringReader)
                reader.beginObject()
                if (reader.nextName() == "Bank") bank.name = reader.nextString()
                if (reader.nextName() == "Strips") {
                    reader.beginArray()
                    while (reader.hasNext()) {
                        reader.beginObject()
                        bank.add(reader.nextName(), reader.nextInt(), true, Track.TrackType.AUDIO)
                        reader.endObject()
                    }
                    reader.endArray()
                }
                reader.endObject()
                banks.add(bank)
                updateBanklist()

            } catch (e: IOException) {
                e.printStackTrace()
            }

            showBank(banks.indexOf(bank))
        }
        dfBankLoad = null
    }

    fun onStripMaskDlg() {

        if (currentBank != null) {
            showBank(currentBank!!.button!!.tag as Int)

            val settings = getSharedPreferences(TAG, 0)
            val editor = settings.edit()

            stripElementMask.saveSettings(editor)


            // editor.commit()
            editor.apply()
        }
    }

    fun getProcessors(iStripIndex: Int) {
        oscService.getProcessors(iStripIndex)
    }

    companion object {

        private const val TAG = "ArdourControl"
    }
}
