/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import android.support.v7.widget.AppCompatButton

/**
 */

class ToggleTextButton : AppCompatButton, ToggleListener {


    private var parentWidth: Float = 0.toFloat()
    private var parentHeight: Float = 0.toFloat()

    private var state = 2
    /*
     * @return the autoToggle
     */
    /*
     * @param autoToggle the autoToggle to set
     */
    var isAutoToggle = false

    internal var untoggledText = "Off"
    internal var toggledText = "On"

    var onColor = Color.WHITE
    private var offColor = Color.BLACK


    private val p: Paint
    /**
     *
     */
    override var toggleState: Boolean
        get() = this.state == 1
        set(s) {

            if (s && state == 1) {
                return
            }
            state = if (s) 1 else 0

            invalidate()
        }

    constructor(context: Context) : super(context) {
        p = Paint(Paint.ANTI_ALIAS_FLAG)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        p = Paint(Paint.ANTI_ALIAS_FLAG)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        p = Paint(Paint.ANTI_ALIAS_FLAG)
    }

    constructor(context: Context, offText: String, onText: String, onColor: Int, offColor: Int) : super(context) {
        untoggledText = offText
        toggledText = onText
        this.onColor = onColor
        this.offColor = offColor
        p = Paint(Paint.ANTI_ALIAS_FLAG)
    }


    override fun onDraw(canvas: Canvas) {
        //super.onDraw(canvas);

        p.style = Paint.Style.FILL
        if (state == 0)
            p.color = offColor
        else
            p.color = onColor
        canvas.drawRoundRect(1f, 1f, parentWidth - 1, parentHeight - 1, 3f, 3f, p)

        //        p.setStyle(Paint.Style.STROKE);
        p.color = onColor
        //        canvas.drawRoundRect(1, 1, parentWidth - 1, parentHeight -1, 3, 3, p);

        val xPos = canvas.width / 2
        val yPos = (canvas.height / 2 - (p.descent() + p.ascent()) / 2).toInt()
        //((textPaint.descent() + textPaint.ascent()) / 2) is the distance from the baseline to the center.

        val text = if (state == 0) untoggledText else toggledText

            val width = p.measureText(text, 0, text.length)

            if (state == 1)
                p.color = offColor
            else if (!isEnabled)
                p.color = 0
            canvas.drawText(
                    if (state == 0) untoggledText else toggledText,
                    xPos - width / 2,
                    yPos.toFloat(),
                    p
            )


    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val parentW = View.MeasureSpec.getSize(widthMeasureSpec)
        val parentH = View.MeasureSpec.getSize(heightMeasureSpec)
        this.setMeasuredDimension(parentW, parentH)
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        parentWidth = parentW.toFloat()
        parentHeight = parentH.toFloat()

    }


    /**
     *
     * Toggle state on click and change the background image
     *
     */
    override fun performClick(): Boolean {

        if (isAutoToggle) {
            toggle()
        }

        return super.performClick()
    }

    /**
     * Method to toggle the state of the button
     */
    override fun toggle() {

        this.toggleState = state != 1

    }

    fun toggleOn() {
        this.toggleState = true
    }

    fun toggleOff() {
        //this.stopBlink();
        this.toggleState = false
    }

    /**
     * @return the untoggledText
     */
    fun getUntoggledText(): String {
        return untoggledText
    }

    /**
     * @param untoggledText the untoggledText to set
     */
    private fun setUntoggledText(untoggledText: String) {
        this.untoggledText = untoggledText
        if (state == 1)
            this.text = untoggledText
    }

    /**
     * @return the toggledText
     */
    fun getToggledText(): String {
        return toggledText
    }

    /**
     * @param toggledText the toggledText to set
     */
    private fun setToggledText(toggledText: String) {
        this.toggledText = toggledText
        if (state == 1)
            this.text = toggledText
    }

    fun setAllText(text: String?) {
        if (text != null) {
            setUntoggledText(text)
            setToggledText(text)
        }

        toggleState = state == 1
    }

    fun setOffColor(offColor: Int) {
        this.offColor = offColor
        //        this.setBackgroundColor(offColor);
    }

}

