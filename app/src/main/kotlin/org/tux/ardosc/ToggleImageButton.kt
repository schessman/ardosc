/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.content.Context
import android.support.v7.widget.AppCompatImageButton
import android.util.AttributeSet
import java.util.concurrent.atomic.AtomicBoolean

class ToggleImageButton : AppCompatImageButton, ToggleListener, Blinkable {

    private var state = false
    /*
     * @return the autoToggle
     */
    /*
     * @param autoToggle the autoToggle to set
     */
    var isAutoToggle = false

    private val canBlink = AtomicBoolean(false)

    /*
     * @return the untoggledResourceId
     */
    /*
     * @param untoggledResourceId the untoggledResourceId to set
     */
    private var untoggledResourceId: Int = 0
    /*
     * @return the toggledResourceId
     */
    /*
     * @param toggledResourceId the toggledResourceId to set
     */
    private var toggledResourceId: Int = 0

    /**
     * Toggle to a given state
     */
    override var toggleState: Boolean
        get() = this.state
        set(s) {

            canBlink.set(false)

            if (s) {
                state = true
                this.setBackgroundResource(toggledResourceId)
            } else {
                state = false
                this.setBackgroundResource(untoggledResourceId)
            }
        }


    constructor(context: Context) : super(context)


    /**
     * @param context
     * @param attrs
     * @param defStyle
     */
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {

        init(context, attrs)
    }


    /**
     * @param context
     * @param attrs
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {

        init(context, attrs)
    }


    /**
     * @param context
     * @param attrs
     */
    private fun init(context: Context, attrs: AttributeSet) {

        val a = context.obtainStyledAttributes(attrs, R.styleable.ToggleImageButton)

        toggledResourceId = a.getResourceId(R.styleable.ToggleImageButton_background_toggled, 0)
        untoggledResourceId = a.getResourceId(R.styleable.ToggleImageButton_background_untoggled, 0)

        a.recycle()

        this.setBackgroundResource(untoggledResourceId)
    }


    /**
     *
     * Toggle state on click and change the background image
     *
     */
    override fun performClick(): Boolean {

        if (isAutoToggle) {
            toggle()
        }

        return super.performClick()
    }


    /**
     * Method to toggle the state of the button
     */
    override fun toggle() {

        state = if (!state) {
            this.setBackgroundResource(toggledResourceId)
            true
        } else {
            this.setBackgroundResource(untoggledResourceId)
            false
        }
    }

    /**
     * Toogle to a given state
     * @param s
     */
    fun setToggleState(s: Boolean, blink: Boolean) {

        state = if (s) {

            this.setBackgroundResource(toggledResourceId)

            if (blink) {
                this.startBlink()
            }

            true
        } else {
            this.setBackgroundResource(untoggledResourceId)
            canBlink.set(false)
            false
        }
    }

    fun toggleOn() {
        this.toggleState = true
    }

    fun toggleOnAndBlink() {
        this.setToggleState(true, true)
    }

    fun toggleOff() {
        //this.stopBlink();
        this.toggleState = false
    }


    override fun blink() {
        if (canBlink.get()) {
            this.toggle()
        }
    }


    override fun startBlink() {
        canBlink.set(true)
    }


    //	@Override
    //	public void stopBlink() {
    //
    //		canBlink.set(false);
    //
    //		if (state){
    //			setToggleState(true);
    //		}
    //		else {
    //			setToggleState(false);
    //		}
    //	}
    //
    //	@Override
    //	public boolean shouldBlink() {
    //		return canBlink.get();
    //	}
}
