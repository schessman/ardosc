/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.support.v4.app.DialogFragment
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout

import java.util.ArrayList

/**
 * class BankLoadDialog - dialog handler
 * @property files ArrayList<String>?
 */

class BankLoadDialog : DialogFragment() {

    private var files: ArrayList<String>? = null

    @SuppressLint("InflateParams")
    override fun onCreateDialog(settingsBundle: Bundle?): Dialog {

        val args = arguments

        files = args!!.getStringArrayList("files")


        // Use the Builder class for convenient dialog construction
        val builder = AlertDialog.Builder(activity)
        val inflater = activity!!.layoutInflater
        val view = inflater.inflate(R.layout.file_dlg, null) as LinearLayout
        val fsl = FileSelectLayout(view.context)
        fsl.setFileNames(files)

        fsl.vonClickListener = View.OnClickListener { v ->
            val tag = v.tag

            val callingActivity = activity as MainActivity?
            callingActivity!!.loadBankFile(tag)
        }

        view.addView(fsl)

        builder.setView(view)


        builder.setNegativeButton(R.string.cancel) { _, _ ->
            // User cancelled the dialog
        }

        return builder.create()
    }
}
