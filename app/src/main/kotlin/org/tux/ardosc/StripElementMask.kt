/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 */

class StripElementMask {
    var bTitle = true
    var bFX = true
    var bSend = true
    var bRecord = true
    var bReceive = true
    var bInput = true

    var bMeter = true
    var bMute = true
    var bSolo = true

    var bPan = true
    var bFader = true
    var bSoloIso = false
    var bSoloSafe = false

    var stripSize = 1

    var autoSize = 32

    fun config(context: AppCompatActivity) {
        val sdlg = StripMaskDialogFragment()
        val settingsBundle = Bundle()

        settingsBundle.putInt("stripSize", stripSize)
        settingsBundle.putBoolean("title", bTitle)
        settingsBundle.putBoolean("fx", bFX)
        settingsBundle.putBoolean("send", bSend)
        settingsBundle.putBoolean("record", bRecord)
        settingsBundle.putBoolean("receive", bReceive)
        settingsBundle.putBoolean("input", bInput)
        settingsBundle.putBoolean("meter", bMeter)
        settingsBundle.putBoolean("mute", bMute)
        settingsBundle.putBoolean("solo", bSolo)
        settingsBundle.putBoolean("soloiso", bSoloIso)
        settingsBundle.putBoolean("solosafe", bSoloSafe)
        settingsBundle.putBoolean("pan", bPan)
        settingsBundle.putBoolean("fader", bFader)

        sdlg.item = this
        sdlg.arguments = settingsBundle

        sdlg.show(context.supportFragmentManager, "Connection Settings")

    }

    fun loadSettings(settings: SharedPreferences) {
        bTitle = settings.getBoolean("mskTitle", true)
        bFX = settings.getBoolean("mskFx", true)
        bSend = settings.getBoolean("mskSend", true)
        bRecord = settings.getBoolean("mskRecord", true)
        bReceive = settings.getBoolean("mskReceive", true)
        bInput = settings.getBoolean("mskInput", true)

        bMeter = settings.getBoolean("mskMeter", true)
        bMute = settings.getBoolean("mskMute", true)
        bSolo = settings.getBoolean("mskSolo", true)
        bSoloIso = settings.getBoolean("mskSoloIso", true)
        bSoloSafe = settings.getBoolean("mskSoloSafe", true)
        bPan = settings.getBoolean("mskPan", true)
        bFader = settings.getBoolean("mskFader", true)

        stripSize = settings.getInt("strip_wide", 1)

    }

    fun saveSettings(editor: SharedPreferences.Editor) {
        editor.putBoolean("mskTitle", bTitle)
        editor.putBoolean("mskMeter", bMeter)
        editor.putBoolean("mskFX", bFX)
        editor.putBoolean("mskSend", bSend)
        editor.putBoolean("mskRecord", bRecord)
        editor.putBoolean("mskReceive", bReceive)
        editor.putBoolean("mskInput", bInput)
        editor.putBoolean("mskSolo", bSolo)
        editor.putBoolean("mskSoloIso", bSoloIso)
        editor.putBoolean("mskSoloSafe", bSoloSafe)
        editor.putBoolean("mskMute", bMute)
        editor.putBoolean("mskPan", bPan)

        editor.putInt("strip_wide", stripSize)

    }
}
