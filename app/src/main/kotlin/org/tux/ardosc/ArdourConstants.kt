/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

/*
 * ArdOSC constants.
 * Could make some of these enums for better type checking.
 */

    // Ardour strip type constants
    const val STRIP_TRACK_AUDIO = 1
    // const val STRIP_TRACK_MIDI = 2
    const val STRIP_BUS_AUDIO = 4
    // const val STRIP_BUS_MIDI = 8
    const val STRIP_VCA = 16
    const val STRIP_MASTER = 32
    // const val STRIP_MONITOR = 64
    const val STRIP_AUX = 128
    // const val STRIP_SELECTED = 256
    const val STRIP_HIDDEN = 512

    // Ardour feedback constants
    const val FEEDBACK_STRIP_BUTTONS = 1
    const val FEEDBACK_STRIP_VALUES = 2
    // const val FEEDBACK_SSID_IN_PATH = 4
    // const val FEEDBACK_HEARTBEAT = 8
    const val FEEDBACK_MASTER = 16
    // const val FEEDBACK_BAR_BEAT = 32
    const val FEEDBACK_TIMECODE = 64
    // const val FEEDBACK_STRIP_METER = 128
    const val FEEDBACK_STRIP_METER_16BIT = 256
    // const val FEEDBACK_STRIP_SIGNAL_PRESENT = 512
    const val FEEDBACK_TRANSPORT_POSITION_SAMPLES = 1024
    // const val FEEDBACK_TRANSPORT_POSITION_TIME = 2048
    const val FEEDBACK_EXTRA_SELECT = 8192

    // Ardour msg.what const values
    const val OSC_STRIP_NAME = 50
    const val OSC_STRIP_REC = 60
    const val OSC_STRIP_MUTE = 70
    const val OSC_STRIP_SOLO = 80
    const val OSC_STRIP_SOLOISO = 81
    const val OSC_STRIP_SOLOSAFE = 82
    const val OSC_STRIP_FADER = 90
    const val OSC_STRIP_PAN = 100
    const val OSC_STRIP_INPUT = 110
    const val OSC_STRIP_SELECT = 300
    const val OSC_MAXFRAMES = 400
    const val OSC_FRAMERATE = 500
    const val OSC_STRIPLIST = 1000
    const val OSC_RECONNECT = 1020


    const val OSC_UPDATE_CLOCK = 3000
    const val OSC_UPDATE_CLOCKSTRING = 3100
    const val OSC_RECORD = 3500
    const val OSC_PLAY = 3600
    const val OSC_STOP = 3700

    const val OSC_STRIP_METER = 6000

    const val OSC_STRIP_SENDS = 7000
    const val OSC_SELECT_SEND_FADER = 7500
    const val OSC_SELECT_SEND_ENABLE = 7600

    const val OSC_STRIP_RECEIVES = 8000
    const val OSC_NEWSTRIP = 1010

    const val OSC_PLUGIN_LIST = 4010
    const val OSC_PLUGIN_DESCRIPTOR = 4000
    const val OSC_PLUGIN_DESCRIPTOR_END = 4020

    const val OSC_SELECT_SEND_NAME = 7100


    // constants about Ardour Transport
    const val TRANSPORT_STOPPED: Byte = 0x01
    const val TRANSPORT_RUNNING: Byte = 0x02
    const val RECORD_ENABLED: Byte = 0x04

