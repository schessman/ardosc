/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.content.Context
import android.graphics.Color
import android.os.Handler
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.support.v4.content.ContextCompat
import android.view.View

import org.tux.ardosc.FaderView.FaderViewListener


/**
 */

class StripLayout : LinearLayout {

    var track: Track? = null
    private var tvStripName: TextView? = null
    private var ttbSends: ToggleTextButton? = null
    private var ttbFX: ToggleTextButton? = null
    private var ttbRecord: ToggleTextButton? = null
    private var ttbInput: ToggleTextButton? = null
    private var ttbMute: ToggleTextButton? = null
    private var ttbSolo: ToggleTextButton? = null
    private var ttbSoloIso: ToggleTextButton? = null
    private var ttbSoloSafe: ToggleTextButton? = null
    private var ttbPan: ToggleTextButton? = null
    private var fwVolume: FaderView? = null
    private var meterImage: MeterImageView? = null
    private var kbPan: RoundKnobButton? = null

    private var onClickListener: View.OnClickListener? = null
    private var onChangeHandler: Handler? = null

    // params for senders
    private var oldVolume: Int = 0

    var showType: Track.TrackType? = null
    var position: Int = 0

    private var stripWide: Int = 0
    private var buttonHeight = 32


    private val faderViewListener = object : FaderViewListener {
        override fun onFader(id: Int, pos: Int) {
            when (showType) {
                Track.TrackType.RECEIVE -> {
                    track!!.currentSendVolume = pos
                    onChangeHandler!!.sendMessage(onChangeHandler!!.obtainMessage(RECEIVE_CHANGED, track!!.sourceId, track!!.currentSendVolume))
                }
                Track.TrackType.SEND -> {
                    track!!.currentSendVolume = pos
                    onChangeHandler!!.sendMessage(onChangeHandler!!.obtainMessage(AUX_CHANGED, id, track!!.trackVolume))
                }
                Track.TrackType.PAN -> {
                    track!!.panPosition = pos.toFloat() / 1000
                    onChangeHandler!!.sendMessage(onChangeHandler!!.obtainMessage(PAN_CHANGED, track!!.remoteId, pos))
                }
                else -> {
                    track!!.trackVolume = pos
                    onChangeHandler!!.sendMessage(onChangeHandler!!.obtainMessage(STRIP_FADER_CHANGED, track!!.remoteId, track!!.trackVolume))
                }
            }
        }

        override fun onStartFade() {
            track!!.trackVolumeOnSeekBar = true
        }

        override fun onStopFade(id: Int, pos: Int) {
            track!!.trackVolumeOnSeekBar = false
        }
    }

    val remoteId: Int
        get() = track!!.remoteId

    constructor(context: Context) : super(context)

    constructor(context: Context, trk: Track) : super(context) {
        this.orientation = LinearLayout.VERTICAL
        this.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.fader))
        this.track = trk
    }


    fun init(context: Context, mask: StripElementMask) {

        removeAllViews()

        when (mask.stripSize) {
            SMALL_STRIP -> {
                stripWide = STRIP_SMALL_WIDTH
                buttonHeight = 28
            }
            MEDIUM_STRIP -> {
                stripWide = STRIP_MEDIUM_WIDTH
                buttonHeight = 32
            }
            WIDE_STRIP -> {
                stripWide = STRIP_WIDE_WIDTH
                buttonHeight = 40
            }
            AUTO_STRIP -> {
                stripWide = mask.autoSize
                buttonHeight = 32
            }
        }
        this.layoutParams.width = stripWide

        val switchLP = LinearLayout.LayoutParams(
                stripWide,
                buttonHeight)

        val switchWLP = LinearLayout.LayoutParams(
                stripWide / 2,
                buttonHeight)

        switchLP.setMargins(1, 1, 0, 0)
        switchWLP.setMargins(1, 1, 0, 0)

        val lpStripName = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                26)
        lpStripName.setMargins(1, 1, 0, 0)
        tvStripName = TextView(context)
        tvStripName!!.setPadding(1, 0, 1, 0)
        tvStripName!!.maxLines = 1
        tvStripName!!.text = track!!.name
        tvStripName!!.layoutParams = lpStripName
        tvStripName!!.id = track!!.remoteId
        tvStripName!!.setTextColor(Color.BLACK)
        tvStripName!!.isClickable = true
        tvStripName!!.id = id
        tvStripName!!.tag = "strip"
        tvStripName!!.setOnClickListener(onClickListener)
        tvStripName!!.gravity = Gravity.CENTER_HORIZONTAL
        when (track!!.type) {
            Track.TrackType.AUDIO -> {
                showType = Track.TrackType.AUDIO
                tvStripName!!.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.AUDIO_STRIP_BACKGROUND))
                setBackgroundColor(ContextCompat.getColor(getContext(), R.color.AUDIO_STRIP_BACKGROUND))
            }
            Track.TrackType.BUS -> {
                showType = Track.TrackType.BUS
                tvStripName!!.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.BUS_AUX_BACKGROUND))
                setBackgroundColor(ContextCompat.getColor(getContext(), R.color.BUS_AUX_BACKGROUND))
            }

            Track.TrackType.MASTER -> {
                showType = Track.TrackType.MASTER
                tvStripName!!.setBackgroundColor(Color.WHITE)
            }
            Track.TrackType.VCA -> {
                showType = Track.TrackType.VCA
                tvStripName!!.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.BUTTON_FX))
            }
            Track.TrackType.MIDI -> {}
            Track.TrackType.SEND -> {}
            Track.TrackType.RECEIVE -> {}
            Track.TrackType.PAN -> {}
            null -> {}
        }
        if (mask.bTitle)
            this.addView(tvStripName)

        if (track!!.type != Track.TrackType.VCA) {
            val meterParam = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    METER_IMAGE_HIGHT)
            meterParam.setMargins(1, 1, 0, 0)
            meterImage = MeterImageView(context)
            meterImage!!.layoutParams = meterParam
            meterImage!!.id = id
            meterImage!!.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.VeryDark))

            if (mask.bMeter)
                this.addView(meterImage)
        }

        ttbFX = ToggleTextButton(context, "FX", "FX", ContextCompat.getColor(getContext(), R.color.BUTTON_FX), R.color.VeryDark)
        ttbFX!!.setPadding(0, 0, 0, 0)
        ttbFX!!.layoutParams = switchLP
        ttbFX!!.toggleState = false
        ttbFX!!.id = id
        ttbFX!!.tag = "fx"
        ttbFX!!.setOnClickListener(onClickListener)
        ttbFX!!.isAutoToggle = true


        ttbSends = ToggleTextButton(context, "AUX", "AUX", ContextCompat.getColor(getContext(), R.color.BUTTON_SEND), R.color.VeryDark)
        ttbSends!!.setPadding(0, 0, 0, 0)
        ttbSends!!.layoutParams = switchLP
        ttbSends!!.toggleState = false
        ttbSends!!.id = id
        ttbSends!!.tag = "aux"
        ttbSends!!.setOnClickListener(onClickListener)
        ttbSends!!.isAutoToggle = true

        if (track!!.type != Track.TrackType.VCA) {
            if (mask.stripSize == WIDE_STRIP) {
                val sendFx = LinearLayout(context)
                sendFx.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                sendFx.orientation = LinearLayout.HORIZONTAL
                if (track!!.type != Track.TrackType.MASTER) {
                    ttbFX!!.layoutParams = switchWLP
                    ttbSends!!.layoutParams = switchWLP
                    if (mask.bFX)
                        sendFx.addView(ttbFX)
                    if (mask.bSend && track!!.type != Track.TrackType.MASTER)
                        sendFx.addView(ttbSends)
                } else {
                    ttbFX!!.layoutParams = switchLP
                    if (mask.bFX)
                        sendFx.addView(ttbFX)
                }
                this.addView(sendFx)
            } else {
                if (mask.bFX)
                    this.addView(ttbFX)
                if (mask.bSend && track!!.type != Track.TrackType.MASTER)
                    this.addView(ttbSends)
            }
        }

        if (track!!.type != Track.TrackType.MASTER) {
            ttbRecord = ToggleTextButton(context, "REC", "REC", ContextCompat.getColor(getContext(), R.color.BUTTON_RECORD), R.color.VeryDark)
            ttbRecord!!.setPadding(0, 0, 0, 0)
            ttbRecord!!.layoutParams = switchLP
            ttbRecord!!.id = id
            if (track!!.type == Track.TrackType.AUDIO || track!!.type == Track.TrackType.MIDI) {
                ttbRecord!!.tag = "rec"
                ttbRecord!!.toggleState = track!!.recEnabled
                ttbRecord!!.setOnClickListener(onClickListener)
            } else if (track!!.type == Track.TrackType.BUS) {
                ttbRecord!!.setAllText("Sof")
                ttbRecord!!.onColor = ContextCompat.getColor(getContext(), R.color.BUS_AUX_BACKGROUND)
                ttbRecord!!.setOffColor(R.color.VeryDark)
                ttbRecord!!.tag = "in"
                ttbRecord!!.toggleState = false
                ttbRecord!!.setOnClickListener(onClickListener)
                ttbRecord!!.isAutoToggle = true
            }


            ttbInput = ToggleTextButton(context, "INPUT", "INPUT", ContextCompat.getColor(getContext(), R.color.BUTTON_INPUT), R.color.VeryDark)
            ttbInput!!.setPadding(0, 0, 0, 0)
            ttbInput!!.layoutParams = switchLP
            ttbInput!!.tag = "input"
            ttbInput!!.id = id
            ttbInput!!.toggleState = track!!.stripIn
            ttbInput!!.setOnClickListener(onClickListener)
            if (mask.stripSize == WIDE_STRIP) {
                val recIn = LinearLayout(context)
                recIn.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                recIn.orientation = LinearLayout.HORIZONTAL
                if (track!!.type == Track.TrackType.AUDIO) {
                    ttbRecord!!.layoutParams = switchWLP
                    ttbInput!!.layoutParams = switchWLP
                    if (mask.bRecord && track!!.type == Track.TrackType.AUDIO || mask.bReceive && track!!.type == Track.TrackType.BUS)
                        recIn.addView(ttbRecord)
                    if (mask.bInput)
                        recIn.addView(ttbInput)
                } else if (track!!.type == Track.TrackType.BUS) {
                    ttbRecord!!.layoutParams = switchLP
                    if (mask.bRecord && track!!.type == Track.TrackType.AUDIO || mask.bReceive && track!!.type == Track.TrackType.BUS)
                        recIn.addView(ttbRecord)
                }
                this.addView(recIn)
            } else {
                if ((mask.bRecord && track!!.type == Track.TrackType.AUDIO || mask.bReceive && track!!.type == Track.TrackType.BUS) && track!!.type != Track.TrackType.MASTER)
                    this.addView(ttbRecord)
                if (mask.bInput && track!!.type == Track.TrackType.AUDIO)
                    this.addView(ttbInput)
            }
        }


        ttbMute = ToggleTextButton(context, "MUTE", "MUTE", ContextCompat.getColor(getContext(), R.color.BUTTON_MUTE), R.color.VeryDark)
        ttbMute!!.setPadding(0, 0, 0, 0)
        ttbMute!!.layoutParams = switchLP
        ttbMute!!.tag = "mute"
        ttbMute!!.id = id
        ttbMute!!.toggleState = track!!.muteEnabled
        ttbMute!!.setOnClickListener(onClickListener)

        ttbSolo = ToggleTextButton(context, "SOLO", "SOLO", ContextCompat.getColor(getContext(), R.color.BUTTON_SOLO), R.color.VeryDark)
        ttbSolo!!.setPadding(0, 0, 0, 0)
        ttbSolo!!.layoutParams = switchLP
        ttbSolo!!.tag = "solo"
        ttbSolo!!.id = id
        ttbSolo!!.toggleState = track!!.soloEnabled
        ttbSolo!!.setOnClickListener(onClickListener)

        if (mask.stripSize == WIDE_STRIP) {
            val muteSolo = LinearLayout(context)
            muteSolo.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            muteSolo.orientation = LinearLayout.HORIZONTAL
            if (track!!.type != Track.TrackType.MASTER) {
                ttbMute!!.layoutParams = switchWLP
                ttbSolo!!.layoutParams = switchWLP
                if (mask.bMute)
                    muteSolo.addView(ttbMute)
                if (mask.bSolo && track!!.type != Track.TrackType.MASTER)
                    muteSolo.addView(ttbSolo)
            } else {
                ttbMute!!.layoutParams = switchLP
                if (mask.bMute)
                    muteSolo.addView(ttbMute)
            }
            this.addView(muteSolo)
        } else {
            if (mask.bMute)
                this.addView(ttbMute)
            if (mask.bSolo && track!!.type != Track.TrackType.MASTER)
                this.addView(ttbSolo)
        }

        if (track!!.type != Track.TrackType.MASTER) {
            ttbSoloIso = ToggleTextButton(context, "Iso", "Iso", ContextCompat.getColor(getContext(), R.color.BUTTON_SOLO), R.color.VeryDark)
            ttbSoloIso!!.setPadding(0, 0, 0, 0)
            ttbSoloIso!!.layoutParams = switchLP
            ttbSoloIso!!.tag = "soloiso"
            ttbSoloIso!!.id = id
            ttbSoloIso!!.toggleState = track!!.soloIsolateEnabled
            ttbSoloIso!!.setOnClickListener(onClickListener)
            if (track!!.type == Track.TrackType.MASTER) {
                ttbSoloIso!!.isEnabled = track!!.soloIsolateEnabled
                ttbSoloIso!!.untoggledText = ""
            }

            ttbSoloSafe = ToggleTextButton(context, "Lock", "Lock", ContextCompat.getColor(getContext(), R.color.BUTTON_SOLO), R.color.VeryDark)
            ttbSoloSafe!!.setPadding(0, 0, 0, 0)
            ttbSoloSafe!!.layoutParams = switchLP
            ttbSoloSafe!!.tag = "solosafe"
            ttbSoloSafe!!.id = id
            ttbSoloSafe!!.toggleState = track!!.soloSafeEnabled
            ttbSoloSafe!!.setOnClickListener(onClickListener)
            if (track!!.type == Track.TrackType.MASTER) {
                ttbSoloSafe!!.isEnabled = track!!.soloSafeEnabled
                ttbSoloSafe!!.untoggledText = ""
            }

            if (mask.stripSize == WIDE_STRIP) {
                val isoSafe = LinearLayout(context)
                isoSafe.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                isoSafe.orientation = LinearLayout.HORIZONTAL
                ttbSoloIso!!.layoutParams = switchWLP
                ttbSoloSafe!!.layoutParams = switchWLP
                if (mask.bSoloIso)
                    isoSafe.addView(ttbSoloIso)
                if (mask.bSoloSafe)
                    isoSafe.addView(ttbSoloSafe)
                this.addView(isoSafe)
            } else {
                if (mask.bSoloIso)
                    this.addView(ttbSoloIso)
                if (mask.bSoloSafe)
                    this.addView(ttbSoloSafe)
            }
        }

        if (track!!.type != Track.TrackType.MASTER && track!!.type != Track.TrackType.VCA) {
            ttbPan = ToggleTextButton(context, "PAN", "PAN", ContextCompat.getColor(getContext(), R.color.BUTTON_PAN), R.color.VeryDark)
            ttbPan!!.setPadding(0, 0, 0, 0)
            ttbPan!!.layoutParams = switchLP
            ttbPan!!.tag = "pan"
            ttbPan!!.id = id
            ttbPan!!.toggleState = false
            ttbPan!!.setOnClickListener(onClickListener)
            ttbPan!!.isAutoToggle = true
            if (track!!.type == Track.TrackType.MASTER) {
                ttbPan!!.isEnabled = false
                ttbPan!!.untoggledText = ""
            }
            if (mask.bPan)
                this.addView(ttbPan)
            else {
                val lp = RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                lp.addRule(RelativeLayout.CENTER_IN_PARENT)
                kbPan = RoundKnobButton(context, R.drawable.stator, R.drawable.knob1, stripWide, stripWide)
                lp.setMargins(2, 2, 0, 0)
                kbPan!!.setListener(object : RoundKnobButton.RoundKnobButtonListener {

                    override fun onRotate(percentage: Int) {
                        track!!.panPosition = percentage.toFloat() / 100
                        onChangeHandler!!.sendMessage(onChangeHandler!!.obtainMessage(PAN_CHANGED, track!!.remoteId, percentage * 10))

                    }

                    override fun onStartRotate() {
                        track!!.trackVolumeOnSeekBar = true
                    }

                    override fun onStopRotate() {
                        track!!.trackVolumeOnSeekBar = false
                    }
                })
                kbPan!!.layoutParams = lp
                kbPan!!.setProgress((track!!.panPosition * 100).toInt())
                this.addView(kbPan)
            }
        }

        val fwLP = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT)
        fwLP.setMargins(1, 1, 0, 0)
        fwVolume = FaderView(context)
        fwVolume!!.layoutParams = fwLP
        fwVolume!!.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.VeryDark))
        fwVolume!!.tag = "volume"
        fwVolume!!.id = id
        fwVolume!!.setListener(faderViewListener)
        fwVolume!!.val0 = 782
        fwVolume!!.progress = track!!.trackVolume
        if (mask.bFader)
            this.addView(fwVolume)

    }

    override fun setOnClickListener(onClickListener: View.OnClickListener?) {
        this.onClickListener = onClickListener
    }

    fun setOnChangeHandler(myHandler: Handler) {
        this.onChangeHandler = myHandler
    }

    fun nameChanged() {
        tvStripName!!.text = track!!.name
    }

    fun recChanged() {
        if (ttbRecord != null)
            ttbRecord!!.toggleState = track!!.recEnabled
    }

    fun muteChanged() {
        if (showType == Track.TrackType.AUDIO || showType == Track.TrackType.BUS || showType == Track.TrackType.MASTER)
            ttbMute!!.toggleState = track!!.muteEnabled

    }

    fun soloChanged() {
        ttbSolo!!.toggleState = track!!.soloEnabled
    }

    fun soloSafeChanged() {
        if (ttbSoloSafe != null) {
            ttbSolo!!.isEnabled = !track!!.soloSafeEnabled
            ttbSoloSafe!!.toggleState = track!!.soloSafeEnabled
        }
    }

    fun soloIsoChanged() {
        if (ttbSoloIso != null)
            ttbSoloIso!!.toggleState = track!!.soloIsolateEnabled
    }


    fun inputChanged() {
        if (ttbInput != null)
            ttbInput!!.toggleState = track!!.stripIn
    }

    fun panChanged() {
        if (showType == Track.TrackType.PAN)
            fwVolume!!.progress = (track!!.panPosition * 1000).toInt()
        else if (kbPan != null)
            kbPan!!.setProgress((track!!.panPosition * 100).toInt())
    }

    fun volumeChanged() {
        if (showType == Track.TrackType.AUDIO || showType == Track.TrackType.BUS || showType == Track.TrackType.MASTER || showType == Track.TrackType.VCA)
            fwVolume!!.progress = track!!.trackVolume
    }

    fun setType(type: Track.TrackType?, sendVolume: Float?, sendId: Int, enabled: Boolean) {
        when (type) {
            Track.TrackType.RECEIVE -> {
                track!!.sourceId = sendId
                fwVolume!!.setProgressColor(ContextCompat.getColor(context, R.color.BUS_AUX_BACKGROUND))
                track!!.currentSendEnable = enabled
                track!!.currentSendVolume = (sendVolume!! * 1000).toInt()
                fwVolume!!.progress = track!!.currentSendVolume
                ttbSolo!!.isEnabled = false
                ttbMute!!.setAllText("Off")
                ttbMute!!.toggledText = "On"
                ttbMute!!.onColor = ContextCompat.getColor(context, R.color.BUS_AUX_BACKGROUND)
                ttbMute!!.setOffColor(R.color.VeryDark)
                ttbMute!!.toggleState = enabled
                ttbMute!!.isAutoToggle = true
            }
            Track.TrackType.SEND -> {
                track!!.sourceId = sendId
                fwVolume!!.setProgressColor(ContextCompat.getColor(context, R.color.BUS_AUX_BACKGROUND))
                track!!.currentSendVolume = (sendVolume!! * 1000).toInt()
                fwVolume!!.progress = track!!.currentSendVolume
                ttbSolo!!.isEnabled = false
                ttbMute!!.setAllText("Off")
                ttbMute!!.toggledText = "On"
                ttbMute!!.onColor = ContextCompat.getColor(context, R.color.BUS_AUX_BACKGROUND)
                ttbMute!!.setOffColor(R.color.VeryDark)
                ttbMute!!.toggleState = enabled
                ttbMute!!.isAutoToggle = true
            }
            Track.TrackType.PAN -> {
                fwVolume!!.setStrTopText("right")
                fwVolume!!.setStrBottomText("left")
                fwVolume!!.setMin(500)
                fwVolume!!.setProgressColor(ContextCompat.getColor(context, R.color.BUTTON_PAN))
                fwVolume!!.val0 = 500
                oldVolume = track!!.trackVolume

            }
            /*
            Track.TrackType.MASTER -> {}
            Track.TrackType.AUDIO -> {}
            Track.TrackType.MIDI -> {}
            Track.TrackType.BUS -> {}
            Track.TrackType.VCA -> {}
            null -> {}
            */
            else -> {
                track!!.sourceId = -1
                fwVolume!!.setProgressColor(ContextCompat.getColor(context, R.color.fader))
                fwVolume!!.setbTopText(false)
                fwVolume!!.progress = track!!.trackVolume
                ttbSolo!!.isEnabled = true
                ttbMute!!.setAllText("Mute")
                ttbMute!!.onColor = ContextCompat.getColor(context, R.color.BUTTON_MUTE)
                ttbMute!!.setOffColor(R.color.VeryDark)
                ttbMute!!.toggleState = track!!.muteEnabled
                ttbMute!!.isAutoToggle = false
                //            track!!.muteEnabled = oldMute;
            }
        }
        showType = type
    }

    fun resetType() {
        setType(track!!.type, 0f, 0, false)
    }

    fun meterChange() {
        meterImage!!.setProgress(track!!.meter)
    }

    fun sendChanged(state: Boolean) {
        ttbSends!!.toggleState = state
    }

    fun resetPan() {
        ttbPan!!.toggleState = false
        fwVolume!!.setbTopText(false)
        fwVolume!!.setbBottomText(false)
        fwVolume!!.setMin(0)
        fwVolume!!.setProgressColor(ContextCompat.getColor(context, R.color.AUDIO_STRIP_BACKGROUND))
        fwVolume!!.val0 = 782
        showType = track!!.type
    }

    fun fxOff() {
        ttbFX!!.toggleState = false
    }

    fun pushVolume() {
        oldVolume = track!!.trackVolume
    }

    fun pullVolume() {
        track!!.trackVolume = oldVolume
        volumeChanged()
    }

    fun resetBackground() {
        if (track!!.type == Track.TrackType.AUDIO)
            setBackgroundColor(ContextCompat.getColor(context, R.color.fader))
        if (track!!.type == Track.TrackType.BUS)
            setBackgroundColor(ContextCompat.getColor(context, R.color.BUS_AUX_BACKGROUND))
        if (track!!.type == Track.TrackType.MASTER)
            setBackgroundColor(ContextCompat.getColor(context, R.color.fader))
    }

    companion object {

        const val STRIP_FADER_CHANGED = 20
        const val AUX_CHANGED = 25
        const val RECEIVE_CHANGED = 26
        const val PAN_CHANGED = 27

        const val SMALL_STRIP = 0
        const val MEDIUM_STRIP = 1
        const val WIDE_STRIP = 2
        const val AUTO_STRIP = 3

        const val STRIP_SMALL_WIDTH = 52
        const val STRIP_MEDIUM_WIDTH = 72
        const val STRIP_WIDE_WIDTH = 96


        private const val METER_IMAGE_HIGHT = 160
    }


}
