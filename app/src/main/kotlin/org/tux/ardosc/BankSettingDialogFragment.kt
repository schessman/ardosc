/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.View
import android.view.WindowManager
import android.widget.CheckBox
import android.widget.EditText
import android.widget.LinearLayout
import java.util.*

/**
 */

class BankSettingDialogFragment : DialogFragment() {

    private var bank: Bank? = null
    private var txtBankName: EditText? = null
    private var bankIndex: Int = 0
    private var routes: HashMap<Int, Track>? = null

    @SuppressLint("InflateParams")
    override fun onCreateDialog(settingsBundle: Bundle?): Dialog {

        val args = arguments

        // Use the Builder class for convenient dialog construction
        val builder = AlertDialog.Builder(activity)
        val dialog = builder.create()
        dialog.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        val inflater = activity!!.layoutInflater
        val view = inflater.inflate(R.layout.bank_dlg, null) as LinearLayout

        bankIndex = args!!.getInt("bankIndex")

        val callingActivity = activity as MainActivity?
        bank = callingActivity!!.getBank(bankIndex)
        txtBankName = view.findViewById<View>(R.id.bankname) as EditText
        txtBankName!!.setText(bank!!.name)
        routes = callingActivity.routes
        val ssl = StripSelectLayout(view.context)
        ssl.setRoutes(routes!!, bank!!)

        ssl.onClickListener = View.OnClickListener { v ->

            val id = v.tag as Int
            val selector = v as CheckBox
            if (!selector.isChecked) {
                bank!!.remove(id)
            } else {
                val t = routes!![id]
                bank!!.add(
                        t!!.name!!,
                        id,
                        true,
                        t.type!!
                )
            }
        }
        view.addView(ssl)

        builder.setView(view)

        builder.setPositiveButton(R.string.Ok) { _, _ ->
            val name = txtBankName!!.text.toString()
            bank!!.name = name

            val vcallingActivity = activity as MainActivity?
            vcallingActivity!!.onBankDlg(bankIndex, bank!!)
        }

        builder.setNegativeButton(R.string.cancel) { _, _ ->
            // User cancelled the dialog
        }

        if (bank!!.button != null) {
            builder.setNeutralButton(R.string.action_removebank) { _, _ ->
                val vcallingActivity = activity as MainActivity?
                vcallingActivity!!.removeBank(bankIndex)
            }
        } else {
            builder.setNeutralButton(R.string.action_loadbanks) { _, _ ->
                val vcallingActivity = activity as MainActivity?
                vcallingActivity!!.loadBank()
            }
        }
        return builder.create()
    }

}
