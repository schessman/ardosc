/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.ImageView.ScaleType
import android.widget.RelativeLayout

/*
File:              RoundKnobButton
Version:           1.0.0
Release Date:      November, 2013
License:           GPL v2
Description:	   A round knob button to control volume and toggle between two states

****************************************************************************
Copyright (C) 2013 Radu Motisan  <radu.motisan@gmail.com>

http://www.pocketmagic.net

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
****************************************************************************/

class RoundKnobButton(context: Context, back: Int, rotor: Int, w: Int, h: Int) : RelativeLayout(context), View.OnTouchListener {

    private val ivRotor: ImageView
    private var mnWidth = 0
    private var mnHeight = 0
    private var percent: Int = 0
    private var max = 100
    private var relative: Float = 0.toFloat()
    private var oldVal: Float = 0.toFloat()

    private var mListener: RoundKnobButtonListener? = null

    interface RoundKnobButtonListener {
        fun onRotate(percentage: Int)
        fun onStartRotate()
        fun onStopRotate()
    }

    fun setListener(l: RoundKnobButtonListener) {
        mListener = l
    }

    init {
        // we won't wait for our size to be calculated, we'll just store out fixed size
        mnWidth = w
        mnHeight = h
        // create stator
        this.setOnTouchListener(this)
        val ivBack = ImageView(context)
        ivBack.setImageResource(back)
        val lpIvBack = RelativeLayout.LayoutParams(
                w, h)
        lpIvBack.setMargins(1, 1, 0, 0)

        addView(ivBack, lpIvBack)

        val srcon = BitmapFactory.decodeResource(context.resources, rotor)
        val scaleWidth = w.toFloat() / srcon.width
        val scaleHeight = h.toFloat() / srcon.height
        val matrix = Matrix()
        matrix.postScale(scaleWidth, scaleHeight)

        val bmpRotor = Bitmap.createBitmap(
                srcon, 0, 0,
                srcon.width, srcon.height, matrix, true)

        // create rotor
        ivRotor = ImageView(context)
        ivRotor.setImageBitmap(bmpRotor)
        val lpIvKnob = RelativeLayout.LayoutParams(w, h)//LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lpIvKnob.setMargins(1, 1, 0, 0)
        addView(ivRotor, lpIvKnob)
    }

    private fun setRotorPosAngle(dg: Float) {
        var deg = dg

        if (deg >= 210 || deg <= 150) {
            if (deg > 180) deg -= 360
            val matrix = Matrix()
            ivRotor.scaleType = ScaleType.MATRIX
            matrix.postRotate(deg, (mnWidth / 2).toFloat(), (mnHeight / 2).toFloat())
            ivRotor.imageMatrix = matrix
        }
    }

    private fun setRotorPercentage(percentage: Int) {
        var posDegree = percentage * 3 - 150
        if (posDegree < 0) posDegree += 360
        setRotorPosAngle(posDegree.toFloat())
    }


    override fun onTouch(v: View, event: MotionEvent): Boolean {

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                if (mListener != null) mListener!!.onStartRotate()
                parent.requestDisallowInterceptTouchEvent(true)

                relative = event.y
                oldVal = percent.toFloat()
            }
            MotionEvent.ACTION_MOVE -> {

                var newVal = (oldVal - (event.y - relative) / 2).toInt()
                if (newVal < 0)
                    newVal = 0
                if (newVal > max)
                    newVal = max
                if (newVal != percent) {
                    setProgress(newVal)
                    onSizeChanged(width, height, 0, 0)
                    if (mListener != null) mListener!!.onRotate(newVal)
                }
            }
            MotionEvent.ACTION_UP -> {
                if (mListener != null) mListener!!.onStopRotate()
                parent.requestDisallowInterceptTouchEvent(false)
            }

            MotionEvent.ACTION_CANCEL -> {
            }
        }
        return true
    }

    fun setProgress(`val`: Int) {
        percent = `val`
        setRotorPercentage(percent)
    }

}
