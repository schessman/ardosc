/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.View
import android.widget.CheckBox
import android.widget.RadioButton

/**
 * class StripMaskDialogFragment
 *
 * @property rbSmall RadioButton?
 * @property rbMedium RadioButton?
 * @property rbWide RadioButton?
 * @property rbAuto RadioButton?
 * @property cbStripTitle CheckBox?
 * @property cbStripFX CheckBox?
 * @property cbStripSend CheckBox?
 * @property cbStripRecord CheckBox?
 * @property cbStripReceive CheckBox?
 * @property cbStripInput CheckBox?
 * @property cbStripMeter CheckBox?
 * @property cbStripMute CheckBox?
 * @property cbStripSolo CheckBox?
 * @property cbStripSoloIso CheckBox?
 * @property cbStripSoloSafe CheckBox?
 * @property cbStripPan CheckBox?
 * @property cbStripFader CheckBox?
 * @property item StripElementMask?
 */
@SuppressLint("InflateParams")
class StripMaskDialogFragment : DialogFragment() {

    private var rbSmall: RadioButton? = null
    private var rbMedium: RadioButton? = null
    private var rbWide: RadioButton? = null
    private var rbAuto: RadioButton? = null

    private var cbStripTitle: CheckBox? = null
    private var cbStripFX: CheckBox? = null
    private var cbStripSend: CheckBox? = null
    private var cbStripRecord: CheckBox? = null
    private var cbStripReceive: CheckBox? = null
    private var cbStripInput: CheckBox? = null
    private var cbStripMeter: CheckBox? = null
    private var cbStripMute: CheckBox? = null
    private var cbStripSolo: CheckBox? = null
    private var cbStripSoloIso: CheckBox? = null
    private var cbStripSoloSafe: CheckBox? = null
    private var cbStripPan: CheckBox? = null
    private var cbStripFader: CheckBox? = null

    var item: StripElementMask? = null

    override fun onCreateDialog(settingsBundle: Bundle?): Dialog {

        val args = arguments

        val builder = AlertDialog.Builder(activity)
        val inflater = activity!!.layoutInflater
        val view = inflater.inflate(R.layout.stripmask_dlg, null)
        builder.setView(view)

        builder.setPositiveButton(R.string.Ok) { _, _ ->
            item!!.bTitle = cbStripTitle!!.isChecked
            item!!.bFX = cbStripFX!!.isChecked
            item!!.bSend = cbStripSend!!.isChecked
            item!!.bRecord = cbStripRecord!!.isChecked
            item!!.bReceive = cbStripReceive!!.isChecked
            item!!.bInput = cbStripInput!!.isChecked
            item!!.bMeter = cbStripMeter!!.isChecked
            item!!.bMute = cbStripMute!!.isChecked
            item!!.bSolo = cbStripSolo!!.isChecked
            item!!.bSoloIso = cbStripSoloIso!!.isChecked
            item!!.bSoloSafe = cbStripSoloSafe!!.isChecked
            item!!.bPan = cbStripPan!!.isChecked
            item!!.bFader = cbStripFader!!.isChecked

            when {
                rbSmall!!.isChecked -> item!!.stripSize = StripLayout.SMALL_STRIP
                rbMedium!!.isChecked -> item!!.stripSize = StripLayout.MEDIUM_STRIP
                rbWide!!.isChecked -> item!!.stripSize = StripLayout.WIDE_STRIP
                rbAuto!!.isChecked -> item!!.stripSize = StripLayout.AUTO_STRIP
            }

            val callingActivity = activity as MainActivity?
            callingActivity!!.onStripMaskDlg()
        }
        builder.setNegativeButton(R.string.cancel) { _, _ ->
            // User cancelled the dialog
        }

        cbStripTitle = view.findViewById<View>(R.id.chkMaskTitle) as CheckBox
        cbStripTitle!!.isChecked = args!!.getBoolean("title")

        cbStripFX = view.findViewById<View>(R.id.chkMaskFX) as CheckBox
        cbStripFX!!.isChecked = args.getBoolean("fx")

        cbStripSend = view.findViewById<View>(R.id.chkMaskSend) as CheckBox
        cbStripSend!!.isChecked = args.getBoolean("send")

        cbStripRecord = view.findViewById<View>(R.id.chkMaskRecord) as CheckBox
        cbStripRecord!!.isChecked = args.getBoolean("record")

        cbStripReceive = view.findViewById<View>(R.id.chkMaskReceive) as CheckBox
        cbStripReceive!!.isChecked = args.getBoolean("receive")

        cbStripInput = view.findViewById<View>(R.id.chkMaskInput) as CheckBox
        cbStripInput!!.isChecked = args.getBoolean("input")

        cbStripMeter = view.findViewById<View>(R.id.chkMaskMeter) as CheckBox
        cbStripMeter!!.isChecked = args.getBoolean("meter")

        cbStripMute = view.findViewById<View>(R.id.chkMaskMute) as CheckBox
        cbStripMute!!.isChecked = args.getBoolean("mute")

        cbStripSolo = view.findViewById<View>(R.id.chkMaskSolo) as CheckBox
        cbStripSolo!!.isChecked = args.getBoolean("solo")

        cbStripSoloIso = view.findViewById<View>(R.id.chkMaskSoloIso) as CheckBox
        cbStripSoloIso!!.isChecked = args.getBoolean("soloiso")

        cbStripSoloSafe = view.findViewById<View>(R.id.chkMaskSoloSafe) as CheckBox
        cbStripSoloSafe!!.isChecked = args.getBoolean("solosafe")

        cbStripPan = view.findViewById<View>(R.id.chkMaskPan) as CheckBox
        cbStripPan!!.isChecked = args.getBoolean("pan")

        cbStripFader = view.findViewById<View>(R.id.chkMaskFader) as CheckBox
        cbStripFader!!.isChecked = args.getBoolean("fader")


        rbSmall = view.findViewById<View>(R.id.small_strips) as RadioButton
        rbMedium = view.findViewById<View>(R.id.medium_strips) as RadioButton
        rbWide = view.findViewById<View>(R.id.wide_strips) as RadioButton
        rbAuto = view.findViewById<View>(R.id.auto_strips) as RadioButton

        when (args.getInt("stripSize")) {
            StripLayout.SMALL_STRIP -> rbSmall!!.isChecked = true
            StripLayout.MEDIUM_STRIP -> rbMedium!!.isChecked = true
            StripLayout.WIDE_STRIP -> rbWide!!.isChecked = true
            StripLayout.AUTO_STRIP -> rbAuto!!.isChecked = true
        }

        builder.setNeutralButton(R.string.action_maskall) { _, _ ->
            item!!.bTitle = true
            item!!.bFX = true
            item!!.bSend = true
            item!!.bRecord = true
            item!!.bReceive = true
            item!!.bInput = false
            item!!.bMeter = true
            item!!.bMute = true
            item!!.bSolo = true
            item!!.bSoloIso = false
            item!!.bSoloSafe = false
            item!!.bPan = true
            item!!.bFader = true

            val callingActivity = activity as MainActivity?
            callingActivity!!.onStripMaskDlg()
        }

        return builder.create()
    }
}
