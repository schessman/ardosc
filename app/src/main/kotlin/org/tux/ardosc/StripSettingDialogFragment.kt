/*
 * Copyright (C) 2018, Samuel S Chessman
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This is a derivative work of ardmix and uses the OSC NetUtil java library.
 * Source is available online at https://gitlab.com/schessman/ardosc/
 *
 * The original java sources for ardmix were developed by Detlef Urban and
 * licensed with the BEERWARE Licence. 
 * It was converted to Kotlin and Android Studio 3.2 in Aug 2018.
 * 
 * The original code can be found at http://www.paraair.de/ardmix/
*/
package org.tux.ardosc

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.View
import android.widget.CheckBox
import android.widget.EditText

/**
 */

class StripSettingDialogFragment : DialogFragment() {

    private var stripIndex: Int = 0
    private var txtStripName: EditText? = null
    private var cbStripIn: CheckBox? = null
    private var cbStripRecord: CheckBox? = null
    private var cbStripMute: CheckBox? = null
    private var cbStripSolo: CheckBox? = null
    private var cbStripSoloIso: CheckBox? = null
    private var cbStripSoloSafe: CheckBox? = null

    @SuppressLint("InflateParams")
    override fun onCreateDialog(settingsBundle: Bundle?): Dialog {

        val args = arguments

        // Use the Builder class for convenient dialog construction
        val builder = AlertDialog.Builder(activity)
        val inflater = activity!!.layoutInflater
        val view = inflater.inflate(R.layout.strip_dlg, null)
        builder.setView(view)

        builder.setPositiveButton(R.string.Ok) { _, _ ->
            val callingActivity = activity as MainActivity?
            callingActivity!!.onStripDlg(stripIndex, txtStripName!!.text.toString(), cbStripIn!!.isChecked, cbStripRecord!!.isChecked,
                    cbStripMute!!.isChecked, cbStripSolo!!.isChecked, cbStripSoloIso!!.isChecked, cbStripSoloSafe!!.isChecked)
        }
        builder.setNegativeButton(R.string.cancel) { _, _ -> }

        txtStripName = view.findViewById<View>(R.id.stripname) as EditText
        txtStripName!!.setText(args!!.getString("stripName"))

        cbStripIn = view.findViewById<View>(R.id.sripin) as CheckBox
        if (args.keySet().contains("stripIn"))
            cbStripIn!!.isChecked = args.getBoolean("stripIn")
        else
            cbStripIn!!.isEnabled = false

        cbStripRecord = view.findViewById<View>(R.id.sriprecord) as CheckBox
        if (args.keySet().contains("stripRecord"))
            cbStripRecord!!.isChecked = args.getBoolean("stripRecord")
        else
            cbStripRecord!!.isEnabled = false

        cbStripMute = view.findViewById<View>(R.id.sripmute) as CheckBox
        if (args.keySet().contains("stripMute"))
            cbStripMute!!.isChecked = args.getBoolean("stripMute")
        else
            cbStripMute!!.isEnabled = false

        cbStripSolo = view.findViewById<View>(R.id.sripsolo) as CheckBox
        if (args.keySet().contains("stripSolo"))
            cbStripSolo!!.isChecked = args.getBoolean("stripSolo")
        else
            cbStripSolo!!.isEnabled = false

        cbStripSoloIso = view.findViewById<View>(R.id.sripsoloiso) as CheckBox
        if (args.keySet().contains("stripSoloIso"))
            cbStripSoloIso!!.isChecked = args.getBoolean("stripSoloIso")
        else
            cbStripSoloIso!!.isEnabled = false

        cbStripSoloSafe = view.findViewById<View>(R.id.sripsolosafe) as CheckBox
        if (args.keySet().contains("stripSoloSafe"))
            cbStripSoloSafe!!.isChecked = args.getBoolean("stripSoloSafe")
        else
            cbStripSoloSafe!!.isEnabled = false

        stripIndex = args.getInt("stripIndex")

        builder.setNeutralButton(R.string.action_processors) { _, _ ->
            val callingActivity = activity as MainActivity?
            callingActivity!!.getProcessors(stripIndex)
        }

        return builder.create()
    }

}
