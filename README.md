# Ardosc
GPL Android App to control Ardour5 via OSC for live mixing and recording.

## Platforms
This app targets Android 5.1.1 Lollipop (API 22) or greater.  It works on my Nexus 7 (2012) tablet.  This app needs to connect to Ardour5 with OSC enabled running on another system.  I use RPi3 running Alpine Linux.  See https://audiopi.blogspot.com/

## License
### Copyright (C) 2018, Samuel S Chessman
### SPDX-License-Identifier: GPL-3.0-or-later
The sources are found at https://gitlab.com/schessman/ardosc/

## Credits
This app is a derivative work of ardmix and uses the OSC NetUtil java library.
The original java sources for ardmix were developed by Detlef Urban and licensed with the
BEERWARE Licence.  It was converted to Kotlin and Android Studio 3.2 in Aug 2018.

The original code and screenshots can be found at http://www.paraair.de/ardmix/


```
/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <onkel@paraair.de> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.        Detlef Urban
 * ----------------------------------------------------------------------------
 */
```

The RoundKnobButton implementation is GPLV2, Copyright (C) 2013 Radu Motisan,  http://www.pocketmagic.net.  See https://github.com/radhoo/android-rotaryknob-view for the original sources.

The java sources for de.sciss.net NetUtil were developed by Hanns Holger Rutz and licensed under LGPL 2.1 or later.

The original sources can be found at https://github.com/Sciss/NetUtil/

 
## Files

* LICENSE - License for distribution
* README.md - this file, basic overview and instructions.
* app/ - directory with the Android Studio sources etc.
* ardmix.iml - module file created by IntelliJ IDEA, stores information about a development module, 
* build.gradle - Top-level gradle build file 
* gradle/ - directory with gradle-wrapper.jar
* gradle.properties - runtime gradle properties, sets Java VM args like memory.
* gradlew - bash script to run gradle from command line
* gradlew.bat - windows bat file to run gradle from command line
* settings.gradle - gradle directive to build app


## Todo
* Cleanup the kotlin conversion to use native idioms.
* Make the reader and writer classes run in their own threads instead of the main thread.
* convert the dialog settings into full activities
* add javadoc where appropriate
* build a parser for the OSC feedback messages, maybe SabreCC
